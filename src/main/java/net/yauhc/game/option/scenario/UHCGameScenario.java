package net.yauhc.game.option.scenario;

import java.util.ArrayList;

import net.yauhc.game.UHCGame;
import net.yauhc.game.option.UHCGameOption;

public abstract class UHCGameScenario extends UHCGameOption {

    private boolean isActive = false;
    private ArrayList<Class<? extends UHCGameScenario>> requireActive;
    private ArrayList<Class<? extends UHCGameScenario>> requireInactive;

    public UHCGameScenario(UHCGame game) {
        super(game);

        this.isActive = false;
        this.requireActive = new ArrayList<>();
        this.requireInactive = new ArrayList<>();
    }

    public void setActive(boolean active) {
        if (!canSetActiveTo(active).isEmpty()) {
            throw new IllegalArgumentException("Conditions not met to set active status of scenario");
        }

        this.isActive = active;
    }

    public boolean isActive() {
        return this.isActive;
    }

    public ArrayList<UHCGameScenario> canSetActiveTo(boolean active) {
        ArrayList<UHCGameScenario> errors = new ArrayList<>();

        for (Class<? extends UHCGameScenario> scenario : this.requireActive) {
            UHCGameScenario ofMngr = this.game.getOptionManager().getScenario(scenario);
            if (ofMngr.isActive()) {
                errors.add(ofMngr);
            }
        }

        for (Class<? extends UHCGameScenario> scenario : this.requireInactive) {
            UHCGameScenario ofMngr = this.game.getOptionManager().getScenario(scenario);
            if (!ofMngr.isActive()) {
                errors.add(ofMngr);
            }
        }

        return errors;
    }
}