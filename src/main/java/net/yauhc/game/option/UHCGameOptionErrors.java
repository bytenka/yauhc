package net.yauhc.game.option;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import net.yauhc.lang.UHCLocStr;

public class UHCGameOptionErrors {
    private UHCGameOption option;
    private ArrayList<UHCLocStr> errors;

    public UHCGameOptionErrors(UHCGameOption option, UHCLocStr[] errors) {
        if (option == null) {
            throw new IllegalArgumentException("Option cannot be null");
        }
        this.option = option;
        this.errors = errors == null ? new ArrayList<>() : new ArrayList<>(Arrays.asList(errors));
    }

    public UHCGameOption getGameOption() {
        return this.option;
    }

    public Collection<UHCLocStr> getErrors() {
        return this.errors;
    }
}