package net.yauhc.game.option.rule;

import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.json.JSONException;
import org.json.JSONObject;

import net.yauhc.game.UHCGame;
import net.yauhc.game.option.UHCGameOptionErrors;
import net.yauhc.game.option.UHCGameOptionException;
import net.yauhc.lang.UHCLocStr;
import net.yauhc.player.UHCPlayer;
import net.yauhc.player.UHCPlayerManager;

public class UHCGameRulePvP extends UHCGameRule implements Listener {

    private static final String CFG_FRIENDLY_FIRE = "friendly_fire";
    private static final String CFG_PVP_TIMER = "pvp_timer";

    private boolean friendlyFire;
    private int pvpTimer;

    private boolean isPvPCurrentlyActive = false;

    public UHCGameRulePvP(UHCGame game) {
        super(game);
        this.game.getPlugin().getServer().getPluginManager().registerEvents(this, this.game.getPlugin());
    }

    public void setFriendlyFire(boolean enabled) {
        this.friendlyFire = enabled;
    }

    public boolean getFriendlyFire() {
        return this.friendlyFire;
    }

    public void setPvPTimer(int seconds) throws UHCGameOptionException {
        if (seconds < 0) {
            throw new UHCGameOptionException("Seconds cannot be negative");
        }
        this.pvpTimer = seconds;
    }

    public int getPvPTimer() {
        return this.pvpTimer;
    }

    @Override
    public JSONObject getConfig() {
        JSONObject obj = new JSONObject();
        obj.put(CFG_FRIENDLY_FIRE, friendlyFire);
        obj.put(CFG_PVP_TIMER, pvpTimer);
        return obj;
    }

    @Override
    public void loadConfig(JSONObject config) throws UHCGameOptionException {
        loadDefaultConfig();
        try {
            setFriendlyFire(config.getBoolean(CFG_FRIENDLY_FIRE));
            setPvPTimer(config.getInt(CFG_PVP_TIMER));
        } catch (JSONException e) {
            throw new UHCGameOptionException("Failed to load pvp config", e);
        }
    }

    @Override
    public void loadDefaultConfig() {
        this.friendlyFire = true;
        this.pvpTimer = 60 * 30;
    }

    @Override
    public void onGameStart() throws UHCGameOptionException {

    }

    @Override
    public void onGameStop() {

    }

    @Override
    public void onGameTickSecond(int second) throws UHCGameOptionException {
        this.isPvPCurrentlyActive = second >= this.pvpTimer;

        if (this.pvpTimer - 5 <= second && second < this.pvpTimer) {
            this.game.getChatManager().sendBroadcastMessage(
                    new UHCLocStr("game", "rule", "pvp", "enabling_in").format(String.valueOf(this.pvpTimer - second)),
                    null, Sound.BLOCK_WOODEN_BUTTON_CLICK_OFF);
        }

        if (second == this.pvpTimer) {
            this.game.getChatManager().sendBroadcastMessage(
                    new UHCLocStr("game", "rule", "pvp", "enabled"),
                    null, Sound.ENTITY_WOLF_GROWL);
        }
    }

    @Override
    public UHCGameOptionErrors getErrors() {
        return null;
    }

    @Override
    public String getUUID() {
        return "pvp";
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onEntityDamageByEntityEventFriendlyFire(EntityDamageByEntityEvent event) {
        if (!this.friendlyFire) {
            UHCPlayerManager playerManager = this.game.getPlayerManager();
            if (event.getDamager().getType() == EntityType.PLAYER && event.getEntity().getType() == EntityType.PLAYER) {
                UHCPlayer damager = playerManager.getPlayer((Player) event.getDamager());
                UHCPlayer damaged = playerManager.getPlayer((Player) event.getEntity());

                if (damager != null && damaged != null && damager.getTeam().equals(damaged.getTeam())) {
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onEntityDamageByEntityEventPvP(EntityDamageByEntityEvent event) {
        if (!this.isPvPCurrentlyActive) {
            UHCPlayerManager playerManager = this.game.getPlayerManager();
            if (event.getDamager().getType() == EntityType.PLAYER && event.getEntity().getType() == EntityType.PLAYER) {
                UHCPlayer damager = playerManager.getPlayer((Player) event.getDamager());
                UHCPlayer damaged = playerManager.getPlayer((Player) event.getEntity());

                if (damager != null && damaged != null) {
                    event.setCancelled(true);
                }
            }
        }
    }
}