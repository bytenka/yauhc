package net.yauhc.game.option.rule;

import org.json.JSONException;
import org.json.JSONObject;

import net.yauhc.game.UHCGame;
import net.yauhc.game.option.UHCGameOptionErrors;
import net.yauhc.game.option.UHCGameOptionException;

public class UHCGameRuleChat extends UHCGameRule {

    public static final String CFG_ALLOW_TEAM_CHAT = "allow_team_chat";

    private boolean allowTeamChat;

    public UHCGameRuleChat(UHCGame game) {
        super(game);
    }

    public void setAllowTeamChat(boolean allowTeamChat) {
        this.allowTeamChat = allowTeamChat;
    }

    public boolean getAllowTeamChat() {
        return this.allowTeamChat;
    }

    @Override
    public JSONObject getConfig() {
        JSONObject obj = new JSONObject();
        obj.put(CFG_ALLOW_TEAM_CHAT, this.allowTeamChat);
        return obj;
    }

    @Override
    public void loadConfig(JSONObject config) throws UHCGameOptionException {
        loadDefaultConfig();
        try {
            setAllowTeamChat(config.getBoolean(CFG_ALLOW_TEAM_CHAT));
        } catch (JSONException e) {
            throw new UHCGameOptionException("Failed to load chat config", e);
        }
    }

    @Override
    public void loadDefaultConfig() {
        this.allowTeamChat = true;
    }

    @Override
    public void onGameStart() throws UHCGameOptionException {
        this.game.getChatManager().setTeamChatActive(this.allowTeamChat);
    }

    @Override
    public void onGameStop() {
        this.game.getChatManager().setTeamChatActive(false);
    }

    @Override
    public void onGameTickSecond(int second) throws UHCGameOptionException {

    }

    @Override
    public UHCGameOptionErrors getErrors() {
        return null;
    }

    @Override
    public String getUUID() {
        return "chat";
    }
    
}