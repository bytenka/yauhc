package net.yauhc.game.option.rule;

import java.util.ArrayList;
import java.util.Random;

import org.json.JSONException;
import org.json.JSONObject;

import net.md_5.bungee.api.ChatColor;
import net.yauhc.game.UHCGame;
import net.yauhc.game.option.UHCGameOptionErrors;
import net.yauhc.game.option.UHCGameOptionException;
import net.yauhc.lang.UHCLocStr;
import net.yauhc.player.UHCPlayer;
import net.yauhc.player.UHCPlayerManager;
import net.yauhc.player.UHCTeam;
import net.yauhc.player.UHCTeamManager;
import net.yauhc.player.UHCPlayer.GameRole;
import net.yauhc.player.UHCTeam.Color;
import net.yauhc.player.UHCTeam.Prefix;
import net.yauhc.player.UHCTeam.Type;

public class UHCGameRuleTeams extends UHCGameRule {

    private static final String CFG_TEAMS_ENABLED = "teams_enabled";
    private static final String CFG_MOLE_TEAMS_ASSIGN_TIME = "mole_teams_assign_time";
    private static final String CFG_MOLE_TEAMS_COUNT = "mole_teams_count";

    private boolean teamsEnabled;
    private int moleTeamsAssignTime;
    private int moleTeamsCount;

    public UHCGameRuleTeams(UHCGame game) {
        super(game);
    }

    public void setTeamsEnabled(boolean teamsEnabled) {
        this.teamsEnabled = teamsEnabled;
    }

    public boolean getTeamsEnabled() {
        return this.teamsEnabled;
    }

    public void setMoleTeamsAssignTime(int moleTeamsAssignTime) throws UHCGameOptionException {
        if (moleTeamsAssignTime < 0) {
            throw new UHCGameOptionException("Mole assignation time cannot be negative");
        }
        this.moleTeamsAssignTime = moleTeamsAssignTime;
    }

    public void setMoleTeamsCount(int moleTeamsCount) throws UHCGameOptionException {
        if (moleTeamsCount < 0) {
            throw new UHCGameOptionException("Mole team count cannot be negative");
        }
        this.moleTeamsCount = moleTeamsCount;
    }

    @Override
    public JSONObject getConfig() {
        JSONObject obj = new JSONObject();
        obj.put(CFG_TEAMS_ENABLED, this.teamsEnabled);
        obj.put(CFG_MOLE_TEAMS_ASSIGN_TIME, this.moleTeamsAssignTime);
        obj.put(CFG_MOLE_TEAMS_COUNT, this.moleTeamsCount);
        return obj;
    }

    @Override
    public void loadConfig(JSONObject config) throws UHCGameOptionException {
        loadDefaultConfig();
        try {
            setTeamsEnabled(config.getBoolean(CFG_TEAMS_ENABLED));
            setMoleTeamsAssignTime(config.getInt(CFG_MOLE_TEAMS_ASSIGN_TIME));
            setMoleTeamsCount(config.getInt(CFG_MOLE_TEAMS_COUNT));
        } catch (JSONException e) {
            throw new UHCGameOptionException("Failed to load teams config", e);
        }
    }

    @Override
    public void loadDefaultConfig() {
        this.teamsEnabled = true;
        this.moleTeamsAssignTime = 60 * 30; // 1h30
        this.moleTeamsCount = 1; // 1 player of each team is a mole
    }

    @Override
    public void onGameStart() throws UHCGameOptionException {
        UHCPlayerManager playerManager = this.game.getPlayerManager();
        for (UHCPlayer player : playerManager.getPlayers()) {
            if (this.teamsEnabled) {
                playerManager.setPlayerRole(player, player.getTeam() == null ? GameRole.SPECTATOR : GameRole.PLAYER);
            } else {
                playerManager.setPlayerRole(player, GameRole.PLAYER);
            }
        }
    }

    @Override
    public void onGameStop() {

    }

    @Override
    public void onGameTickSecond(int second) throws UHCGameOptionException {
        if (this.teamsEnabled && this.moleTeamsCount > 0) {
            if (this.moleTeamsAssignTime - 5 <= second && second < this.moleTeamsAssignTime) {
                // Notify reveal soon
            }

            UHCTeamManager teamManager = this.game.getTeamManager();
            if (second == this.moleTeamsAssignTime) {
                Random r = new Random();
                ArrayList<UHCTeam> moleTeams = new ArrayList<>(this.moleTeamsCount);
                Prefix[] prefixes = Prefix.values();
                Color[] colors = Color.values();

                for (int i = 0; i < this.moleTeamsCount; i++) {
                    // Generate a random mole team
                    UHCTeam moleTeam;
                    do {
                        moleTeam = new UHCTeam(Type.MOLE, prefixes[r.nextInt(prefixes.length)],
                                colors[r.nextInt(colors.length)]);
                    } while (moleTeams.contains(moleTeam));
                    moleTeams.add(moleTeam);

                    // Assign a player from each team to the mole team
                    for (UHCTeam team : teamManager.getTeams()) {
                        UHCPlayer[] playersOfTeam = teamManager.getTeamPlayers(team).toArray(new UHCPlayer[0]);
                        int mole = r.nextInt(playersOfTeam.length);
                        teamManager.setPlayerMoleTeam(playersOfTeam[mole], moleTeam);
                    }
                }
            }
        }
    }

    @Override
    public UHCGameOptionErrors getErrors() {
        ArrayList<UHCLocStr> errors = new ArrayList<>();

        if (this.moleTeamsCount > 0) {
            UHCTeamManager teamManager = this.game.getTeamManager();
            for (UHCTeam team : teamManager.getTeams()) {
                if (teamManager.getTeamPlayers(team).size() < this.moleTeamsCount + 2) {
                    errors.add(new UHCLocStr("game", "rule", "teams", "error", "moles_enabled_but_not_enough_players")
                            .format(new UHCLocStr(new UHCLocStr(UHCTeam.colorToChatColor(team.getColor()).toString()),
                                    new UHCLocStr(UHCTeam.prefixToString(team.getPrefix()) + " "),
                                    new UHCLocStr("game", "team", "color", UHCTeam.colorToString(team.getColor())),
                                    new UHCLocStr(ChatColor.RESET.toString()))));
                }
            }
        }

        return errors.isEmpty() ? null : new UHCGameOptionErrors(this, errors.toArray(new UHCLocStr[0]));
    }

    @Override
    public String getUUID() {
        return "teams";
    }

}