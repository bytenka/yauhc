package net.yauhc.game.option.rule;

import net.yauhc.game.UHCGame;
import net.yauhc.game.option.UHCGameOption;

public abstract class UHCGameRule extends UHCGameOption {

    public UHCGameRule(UHCGame game) {
        super(game);
    }

}