package net.yauhc.game.option.rule;

import java.util.ArrayList;

import org.bukkit.WorldBorder;
import org.json.JSONException;
import org.json.JSONObject;

import net.yauhc.game.UHCGame;
import net.yauhc.game.option.UHCGameOptionErrors;
import net.yauhc.game.option.UHCGameOptionException;
import net.yauhc.lang.UHCLocStr;

public class UHCGameRuleWorldBorder extends UHCGameRule {

    private static final String CFG_START_SIZE = "start_size";
    private static final String CFG_END_SIZE = "end_size";
    private static final String CFG_DAMAGE = "damage";
    private static final String CFG_REDUCTION_START = "reduction_start";
    private static final String CFG_REDUCTION_END = "reduction_end";
    private static final String CFG_FINAL_REDUCTION_START = "final_reduction_start";
    private static final String CFG_FINAL_REDUCTION_END = "final_reduction_end";

    private int startSize;
    private int endSize;
    private double damage;
    private int reductionStart;
    private int reductionEnd;
    private int finalReductionStart;
    private int finalReductionEnd;

    public UHCGameRuleWorldBorder(UHCGame game) {
        super(game);
    }

    public int getStartSize() {
        return this.startSize;
    }

    public void setStartSize(int startSize) throws UHCGameOptionException {
        if (startSize < 0) {
            throw new UHCGameOptionException("Start border size cannot be negative");
        }

        this.startSize = startSize;
    }

    public int getEndSize() {
        return this.endSize;
    }

    public void setEndSize(int endSize) throws UHCGameOptionException {
        if (endSize < 0) {
            throw new UHCGameOptionException("Final border size cannot be negative");
        }
        this.endSize = endSize;
    }

    public double getDamage() {
        return this.damage;
    }

    public void setDamage(double damage) throws UHCGameOptionException {
        if (damage < 0) {
            throw new UHCGameOptionException("Border damage cannot be negative");
        }
        this.damage = damage;
    }

    public int getReductionStart() {
        return this.reductionStart;
    }

    public void setReductionStart(int reductionStart) throws UHCGameOptionException {
        if (reductionStart < 0) {
            throw new UHCGameOptionException("Reduction start cannot be negative");
        }

        this.reductionStart = reductionStart;
    }

    public int getReductionEnd() {
        return this.reductionEnd;
    }

    public void setReductionEnd(int reductionEnd) throws UHCGameOptionException {
        if (reductionEnd < 0) {
            throw new UHCGameOptionException("Reduction end cannot be negative");
        }

        this.reductionEnd = reductionEnd;
    }

    public int getFinalReductionStart() {
        return this.finalReductionStart;
    }

    public void setFinalReductionStart(int finalReductionStart) throws UHCGameOptionException {
        if (finalReductionStart < 0) {
            throw new UHCGameOptionException("Final reduction start cannot be negative");
        }

        this.finalReductionStart = finalReductionStart;
    }

    public int getFinalReductionEnd() {
        return this.finalReductionEnd;
    }

    public void setFinalReductionEnd(int finalReductionEnd) throws UHCGameOptionException {
        if (finalReductionEnd < 0) {
            throw new UHCGameOptionException("Final reduction end cannot be negative");
        }

        this.finalReductionEnd = finalReductionEnd;
    }

    @Override
    public JSONObject getConfig() {
        JSONObject obj = new JSONObject();
        obj.put(CFG_START_SIZE, this.startSize);
        obj.put(CFG_END_SIZE, this.endSize);
        obj.put(CFG_DAMAGE, this.damage);
        obj.put(CFG_REDUCTION_START, this.reductionStart);
        obj.put(CFG_REDUCTION_END, this.reductionEnd);
        obj.put(CFG_FINAL_REDUCTION_START, this.finalReductionStart);
        obj.put(CFG_FINAL_REDUCTION_END, this.finalReductionEnd);
        return obj;
    }

    @Override
    public void loadConfig(JSONObject config) throws UHCGameOptionException {
        loadDefaultConfig();
        try {
            setStartSize(config.getInt(CFG_START_SIZE));
            setEndSize(config.getInt(CFG_END_SIZE));
            setDamage(config.getDouble(CFG_DAMAGE));
            setReductionStart(config.getInt(CFG_REDUCTION_START));
            setReductionEnd(config.getInt(CFG_REDUCTION_END));
            setFinalReductionStart(config.getInt(CFG_FINAL_REDUCTION_START));
            setFinalReductionEnd(config.getInt(CFG_FINAL_REDUCTION_END));
        } catch (JSONException e) {
            throw new UHCGameOptionException("Failed to load world border config", e);
        }
    }

    @Override
    public void loadDefaultConfig() {
        this.startSize = 1500; // -750 to +750
        this.endSize = 100; // -50 to +50
        this.damage = 0.5; // 0.5 heart per second per block
        this.reductionStart = 60 * 60; // 1h
        this.reductionEnd = 60 * (60 + 20); // 1h20
        this.finalReductionStart = 60 * (60 + 30); // 1h30
        this.finalReductionEnd = 60 * (60 + 40); // 1h40
    }

    @Override
    public void onGameStart() throws UHCGameOptionException {
        onGameStop();
    }

    @Override
    public void onGameStop() {
        WorldBorder wb = this.game.getWorldManager().getWorld().getWorld().getWorldBorder();
        wb.setCenter(0, 0);
        wb.setDamageAmount(this.damage);
        wb.setDamageBuffer(2);
        wb.setSize(this.startSize);
        wb.setWarningDistance(5);
        wb.setWarningTime(10);
    }

    @Override
    public void onGameTickSecond(int second) throws UHCGameOptionException {
        // TODO notify players
        WorldBorder wb = this.game.getWorldManager().getWorld().getWorld().getWorldBorder();

        if (second == this.reductionStart) {
            wb.setSize(this.endSize, this.reductionEnd - this.reductionStart);
        }

        if (second == this.reductionEnd) {
            wb.setDamageBuffer(2);
            wb.setWarningDistance(0);
        }
    }

    @Override
    public UHCGameOptionErrors getErrors() {
        ArrayList<UHCLocStr> errors = new ArrayList<>();

        if (this.reductionStart > this.reductionEnd) {
            errors.add(new UHCLocStr("game", "rule", "world_border", "error", "reduction_start_after_reduction_end"));
        }

        if (this.reductionEnd > this.finalReductionStart) {
            errors.add(new UHCLocStr("game", "rule", "world_border", "error", "reduction_end_after_final_reduction_start"));
        }

        if (this.finalReductionStart > this.finalReductionEnd) {
            errors.add(new UHCLocStr("game", "rule", "world_border", "error", "final_reduction_start_after_final_reduction_end"));
        }

        return errors.isEmpty() ? null : new UHCGameOptionErrors(this, errors.toArray(new UHCLocStr[0]));
    }

    @Override
    public String getUUID() {
        return "world_border";
    }
}