package net.yauhc.game.option;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;

import org.json.JSONObject;
import org.json.JSONTokener;

import net.yauhc.game.UHCGame;
import net.yauhc.game.option.rule.UHCGameRule;
import net.yauhc.game.option.rule.UHCGameRuleChat;
import net.yauhc.game.option.rule.UHCGameRulePvP;
import net.yauhc.game.option.rule.UHCGameRuleTeams;
import net.yauhc.game.option.rule.UHCGameRuleWorldBorder;
import net.yauhc.game.option.scenario.UHCGameScenario;
import net.yauhc.lang.UHCLocStr;

public class UHCGameOptionManager {

    private static final String CFG_RULES = "rules";
    private static final String CFG_SCENARIOS = "scenarios";

    private UHCGame game;
    private LinkedHashMap<Class<? extends UHCGameRule>, UHCGameRule> rules;
    private LinkedHashMap<Class<? extends UHCGameScenario>, UHCGameScenario> scenarios;

    public UHCGameOptionManager(UHCGame game) {
        if (game == null) {
            throw new IllegalArgumentException("Game cannot be null");
        }
        this.game = game;

        this.rules = new LinkedHashMap<>();
        this.scenarios = new LinkedHashMap<>();

        addRule(new UHCGameRulePvP(this.game));
        addRule(new UHCGameRuleChat(this.game));
        addRule(new UHCGameRuleTeams(this.game));
        addRule(new UHCGameRuleWorldBorder(this.game));
    }

    public void onGameStart() throws UHCGameOptionException {
        for (UHCGameRule rule : this.rules.values()) {
            rule.onGameStart();
        }

        for (UHCGameScenario scenario : this.scenarios.values()) {
            scenario.onGameStart();
        }
    }

    public void onGameStop() {
        for (UHCGameScenario scenario : this.scenarios.values()) {
            scenario.onGameStop();
        }

        for (UHCGameRule rule : this.rules.values()) {
            rule.onGameStop();
        }
    }

    public void onGameTickSecond(int second) throws UHCGameOptionException {
        for (UHCGameRule rule : this.rules.values()) {
            rule.onGameTickSecond(second);
        }

        for (UHCGameScenario scenario : this.scenarios.values()) {
            scenario.onGameTickSecond(second);
        }
    }

    public void loadConfig(String configName) throws UHCGameOptionException {
        File folder = this.game.getPlugin().getDataFolder();
        folder.mkdir();
        
        
        try (FileReader fr = new FileReader(new File(folder, configName + ".json"))) {
            JSONTokener tokener = new JSONTokener(fr);
            JSONObject config = new JSONObject(tokener);

            JSONObject rules = config.optJSONObject(CFG_RULES);
            
            for (UHCGameRule rule : this.rules.values()) {
                JSONObject ruleConfig = rules.optJSONObject(rule.getUUID());
                if (ruleConfig != null) {
                    rule.loadConfig(ruleConfig);
                } else {
                    rule.loadDefaultConfig();
                }
            }
            
            JSONObject scenarios = config.optJSONObject(CFG_SCENARIOS);
            for (UHCGameScenario scenario : this.scenarios.values()) {
                JSONObject scenarioConfig = scenarios.optJSONObject(scenario.getUUID());
                if (scenarioConfig != null) {
                    scenario.loadConfig(scenarioConfig);
                } else {
                    scenario.loadDefaultConfig();
                }
            }

        } catch (IOException e) {
            throw new UHCGameOptionException("Failed to save config", e);
        }

    }

    public void saveConfig(String configName) throws UHCGameOptionException {
        JSONObject config = new JSONObject();

        JSONObject rulesConfig = new JSONObject();
        for (UHCGameRule rule : this.rules.values()) {
            rulesConfig.put(rule.getUUID(), rule.getConfig());
        }
        config.put(CFG_RULES, rulesConfig);

        JSONObject scenariosConfig = new JSONObject();
        for (UHCGameScenario scenario : this.scenarios.values()) {
            scenariosConfig.put(scenario.getUUID(), scenario.getConfig());
        }
        config.put(CFG_SCENARIOS, scenariosConfig);

        File folder = this.game.getPlugin().getDataFolder();
        folder.mkdir();
        try (FileWriter fw = new FileWriter(new File(folder, configName + ".json"))) {
            fw.write(config.toString(4));
        } catch (IOException e) {
            throw new UHCGameOptionException("Failed to save config", e);
        }
    }

    public Collection<UHCLocStr> getRulesErrors() {
        // TODO something better
        ArrayList<UHCLocStr> errors = new ArrayList<>();
        for (UHCGameRule rule : this.rules.values()) {
            UHCGameOptionErrors e = rule.getErrors();
            if (e != null)
                errors.addAll(e.getErrors());
        }
        return errors;
    }

    public Collection<UHCLocStr> getScenariosErrors() {
        // TODO something better
        ArrayList<UHCLocStr> errors = new ArrayList<>();
        for (UHCGameScenario scenario : this.scenarios.values()) {
            UHCGameOptionErrors e = scenario.getErrors();
            if (e != null)
                errors.addAll(e.getErrors());
        }
        return errors;
    }

    public <T extends UHCGameRule> T getRule(Class<T> ruleClass) {
        // This is safe but Java thinks otherwise
        @SuppressWarnings("unchecked")
        T rule = (T) this.rules.get(ruleClass);

        if (rule == null) {
            throw new IllegalArgumentException("Rule not found in manager");
        }

        return rule;
    }

    public <T extends UHCGameScenario> T getScenario(Class<T> scenarioClass) {
        // This is safe but Java thinks otherwise
        @SuppressWarnings("unchecked")
        T scenario = (T) this.scenarios.get(scenarioClass);

        if (scenario == null) {
            throw new IllegalArgumentException("Scenario not found in manager");
        }

        return scenario;
    }

    private void addRule(UHCGameRule rule) {
        if (this.rules.containsKey(rule.getClass())) {
            throw new IllegalArgumentException("Duplicate rule");
        }

        this.rules.put(rule.getClass(), rule);
    }

    private void addScenario(UHCGameScenario scenario) {
        if (this.scenarios.containsKey(scenario.getClass())) {
            throw new IllegalArgumentException("Duplicate scenario");
        }

        this.scenarios.put(scenario.getClass(), scenario);
    }

}