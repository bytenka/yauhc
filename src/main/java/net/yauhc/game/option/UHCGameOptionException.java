package net.yauhc.game.option;

public class UHCGameOptionException extends Exception {

    private static final long serialVersionUID = -8871885748075983158L;

    public UHCGameOptionException(String message) {
        super(message);
    }
   
    public UHCGameOptionException(String message, Throwable cause) {
        super(message, cause);
    }
}