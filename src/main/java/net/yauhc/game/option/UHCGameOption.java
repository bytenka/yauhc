package net.yauhc.game.option;

import org.json.JSONObject;

import net.yauhc.game.UHCGame;

public abstract class UHCGameOption {

    protected UHCGame game;

    public UHCGameOption(UHCGame game) {
        if (game == null) {
            throw new IllegalArgumentException("Game cannot be null");
        }
        this.game = game;

        loadDefaultConfig();
    }

    public abstract void onGameStart() throws UHCGameOptionException;

    public abstract void onGameStop();

    public abstract void onGameTickSecond(int second) throws UHCGameOptionException;

    public abstract UHCGameOptionErrors getErrors();

    public abstract String getUUID();

    public abstract void loadConfig(JSONObject config) throws UHCGameOptionException;

    public abstract void loadDefaultConfig();

    public abstract JSONObject getConfig();

}