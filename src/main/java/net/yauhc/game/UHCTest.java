package net.yauhc.game;

import java.util.Collection;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

public class UHCTest implements Listener {
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockBreakEvent(BlockBreakEvent event) {
        event.setCancelled(true);

        Block block = event.getBlock();
        Player player = event.getPlayer();
        Collection<ItemStack> normalDropItems = block.getDrops(player.getInventory().getItemInMainHand(), player);
        
        World world = event.getBlock().getWorld();
        world.getBlockAt(event.getBlock().getLocation()).setType(Material.AIR);
        
        Collection<ItemStack> toDropItems = getDrops(block, player, normalDropItems);
        int toDropXP = getXP(block, player, event.getExpToDrop());
        
        Location loc = block.getLocation();
        for (ItemStack item : toDropItems) {
            world.dropItemNaturally(loc, item);
        }

        ExperienceOrb orb = world.spawn(loc, ExperienceOrb.class);
        orb.setExperience(toDropXP);
    }

    private Collection<ItemStack> getDrops(Block block, Player player, Collection<ItemStack> requestedDrops) {
        return requestedDrops;
    }

    private int getXP(Block block, Player player, int requestedXP) {
        return requestedXP;
    }
}