package net.yauhc.game;

import java.util.ArrayList;
import java.util.Collection;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import net.yauhc.chat.UHCChatManager;
import net.yauhc.menu.UHCMenuListener;
import net.yauhc.menu.UHCMenuManager;
import net.yauhc.game.option.UHCGameOptionException;
import net.yauhc.game.option.UHCGameOptionManager;
import net.yauhc.game.phase.UHCGamePhase;
import net.yauhc.game.phase.lobby.UHCGamePhaseLobby;
import net.yauhc.game.phase.started.UHCGamePhaseStarted;
import net.yauhc.game.phase.starting.UHCGamePhaseStarting;
import net.yauhc.lang.UHCLocStr;
import net.yauhc.lang.UHCLocaleContainer;
import net.yauhc.lang.UHCLocale;
import net.yauhc.main.UHCPlugin;
import net.yauhc.player.UHCPlayer;
import net.yauhc.player.UHCPlayerConnectionListener;
import net.yauhc.player.UHCPlayerManager;
import net.yauhc.player.UHCTeamManager;
import net.yauhc.world.UHCWorldManager;

public class UHCGame {
    private UHCPlugin plugin;

    private UHCPlayerManager playerManager;
    private UHCTeamManager teamManager;
    private UHCGameOptionManager optionManager;
    private UHCMenuManager menuManager;
    private UHCChatManager chatManager;
    private UHCWorldManager worldManager;

    private ArrayList<UHCGamePhase> phases;
    private int currentPhaseIndex;
    private UHCGameClock clock;
    private int clockID;

    public UHCGame(UHCPlugin plugin) {
        if (plugin == null) {
            throw new IllegalArgumentException("Plugin cannot be null");
        }
        this.plugin = plugin;

        this.playerManager = new UHCPlayerManager(this);
        this.teamManager = new UHCTeamManager(this);
        this.optionManager = new UHCGameOptionManager(this);
        this.menuManager = new UHCMenuManager(this);
        this.chatManager = new UHCChatManager(this);
        this.worldManager = new UHCWorldManager(this);

        this.plugin.getServer().getPluginManager().registerEvents(new UHCPlayerConnectionListener(this), this.plugin);
        this.plugin.getServer().getPluginManager().registerEvents(new UHCMenuListener(this), this.plugin);

        this.phases = new ArrayList<>();
        this.phases.add(new UHCGamePhaseLobby(this));
        this.phases.add(new UHCGamePhaseStarting(this));
        this.phases.add(new UHCGamePhaseStarted(this));

        this.currentPhaseIndex = -1;

        this.clock = new UHCGameClock(this);
        this.clockID = -1;

        stop();
    }

    public void start(CommandSender sender) {
        if (isStarted()) {
            throw new IllegalStateException("The game has already started");
        }

        UHCPlayer playerSender = this.playerManager.getPlayer(this.plugin.getServer().getPlayer(sender.getName()));
        UHCLocale tr = playerSender == null ? UHCLocaleContainer.getLocale(null) : playerSender.getLocale();

        boolean canStart = true;

        Collection<UHCLocStr> ruleErrors = this.optionManager.getRulesErrors();
        if (!ruleErrors.isEmpty()) {
            // TODO rule errors have been found
            canStart = false;
        }

        Collection<UHCLocStr> scenarioErrors = this.optionManager.getScenariosErrors();
        if (!scenarioErrors.isEmpty()) {
            // TODO scenarios errors have been found
            canStart = false;
        }

        if (!canStart) {
            ArrayList<String> msgs = new ArrayList<>();
            msgs.add(ChatColor.RED + new UHCLocStr("game", "error", "config").localize(tr).toString());
            // There is some errors
            if (!ruleErrors.isEmpty()) {
                msgs.add(new UHCLocStr("game", "error", "rule").localize(tr).toString());
                for (UHCLocStr str : ruleErrors) {
                    msgs.add(String.format("%s - %s%s", ChatColor.AQUA + "" + ChatColor.BOLD, ChatColor.RESET,
                            str.localize(tr).toString()));
                }
            }

            if (!scenarioErrors.isEmpty()) {
                msgs.add(new UHCLocStr("game", "error", "scenario").localize(tr).toString());
                for (UHCLocStr str : scenarioErrors) {
                    msgs.add(String.format("%s - %s%s", ChatColor.AQUA + "" + ChatColor.BOLD, ChatColor.RESET,
                            str.localize(tr).toString()));
                }
            }

            sender.sendMessage(msgs.toArray(new String[0]));
        } else {
            try {
                this.optionManager.onGameStart();
                nextPhase();
                this.clockID = Bukkit.getScheduler().scheduleSyncRepeatingTask(this.plugin, this.clock, 1L, 20L);
            } catch (UHCGameOptionException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void stop() {
        if (this.clockID != -1) {
            Bukkit.getScheduler().cancelTask(this.clockID);
        }

        this.optionManager.onGameStop();
        this.currentPhaseIndex = -1;
        nextPhase();
    }

    public boolean isStarted() {
        return this.currentPhaseIndex > 0;
    }

    public UHCPlugin getPlugin() {
        return this.plugin;
    }

    public UHCPlayerManager getPlayerManager() {
        return this.playerManager;
    }

    public UHCTeamManager getTeamManager() {
        return this.teamManager;
    }

    public UHCGameOptionManager getOptionManager() {
        return this.optionManager;
    }

    public UHCMenuManager getMenuManager() {
        return this.menuManager;
    }

    public UHCChatManager getChatManager() {
        return this.chatManager;
    }

    public UHCWorldManager getWorldManager() {
        return this.worldManager;
    }

    public UHCGamePhase getCurrentPhase() {
        return this.phases.get(this.currentPhaseIndex);
    }

    public void nextPhase() {
        if (this.currentPhaseIndex >= this.phases.size() - 1) {
            throw new IllegalStateException("There is no phase left to advance to");
        }

        this.clock.reset();

        if (this.currentPhaseIndex >= 0)
            this.phases.get(this.currentPhaseIndex).onEnd();

        this.currentPhaseIndex++;

        this.phases.get(this.currentPhaseIndex).onBegin();
    }
}