package net.yauhc.game;

import net.yauhc.game.option.UHCGameOptionException;

public class UHCGameClock implements Runnable {

    private UHCGame game;
    private int second;

    public UHCGameClock(UHCGame game) {
        if (game == null) {
            throw new IllegalArgumentException("Game cannot be null");
        }
        this.game = game;

        reset();
    }

    public void reset() {
        this.second = 0;
    }

    @Override
    public void run() {
        try {
            this.game.getCurrentPhase().onTickSecond(this.second);
            this.game.getOptionManager().onGameTickSecond(this.second);
        } catch (UHCGameOptionException e) {
            throw new RuntimeException(e);
        } finally {
            this.second++;
        }
    }

}