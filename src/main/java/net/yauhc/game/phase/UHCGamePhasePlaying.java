package net.yauhc.game.phase;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import net.yauhc.game.UHCGame;
import net.yauhc.game.option.UHCGameOption;
import net.yauhc.player.UHCPlayer;
import net.yauhc.player.UHCTeam;

public abstract class UHCGamePhasePlaying extends UHCGamePhase {

    protected UHCModulePlaying modulePlaying;

    public UHCGamePhasePlaying(UHCGame game) {
        super(game);

        this.modulePlaying = new UHCModulePlaying(this.game, false);
    }

    @Override
    public final void onPlayerJoin(Player player) {
        // TODO player reconnection system
        UHCPlayer p = this.game.getPlayerManager().getPlayer(player);
        p.setPlayer(player);
        onPlayerConnect(p);
    }

    @Override
    public final void onPlayerQuit(Player player) {
        // TODO player reconnection system        

        UHCPlayer p = this.game.getPlayerManager().getPlayer(player);
        p.setPlayer(player);
        onPlayerDisconnect(p);
    }

    @Override
    public final void onPlayerConnect(UHCPlayer player) {
        // TODO player reconnection system
        Bukkit.broadcastMessage(player.toString() + " " + player.getPlayer().getUniqueId().toString());
    }

    @Override
    public final void onPlayerDisconnect(UHCPlayer player) {
        // TODO player reconnection system
        Bukkit.broadcastMessage(player.toString() + " " + player.getPlayer().getUniqueId().toString());
    }

    @Override
    public final void onPlayerChangeTeam(UHCPlayer player, UHCTeam team) {
        throw new IllegalStateException("Player changed team while the game was running");
    }

    @Override
    public final void onOptionUpdate(UHCGameOption option) {
        throw new IllegalStateException("Rules modified while the game was running");
    }

}