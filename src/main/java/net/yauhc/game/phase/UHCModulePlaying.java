package net.yauhc.game.phase;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import net.yauhc.game.UHCGame;
import net.yauhc.main.UHCModule;
import net.yauhc.player.UHCPlayer;
import net.yauhc.player.UHCPlayerManager;
import net.yauhc.player.UHCPlayer.GameRole;
import net.yauhc.world.UHCWorld;

public class UHCModulePlaying extends UHCModule implements Listener {
    public UHCModulePlaying(UHCGame game, boolean active) {
        super(game, active);
        this.game.getPlugin().getServer().getPluginManager().registerEvents(this, this.game.getPlugin());
    }

    // Prevent spectators from going too far from the border
    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerMoveEvent(PlayerMoveEvent event) {
        if (isActive()) {
            UHCPlayerManager playerManager = this.game.getPlayerManager();
            UHCPlayer player = playerManager.getPlayer(event.getPlayer());
            if (player != null && player.getRole() == GameRole.SPECTATOR) {
                UHCWorld world = this.game.getWorldManager().getWorld();
                // Small offset cause its frustrating otherwise
                double wbSize = (world.getWorld().getWorldBorder().getSize() / 2) + 10; 
                double radiusMax2 = Math.pow(wbSize, 2) * 2;
                double playerDist2 = Math.pow(event.getTo().getX(), 2) + Math.pow(event.getTo().getZ(), 2);
                if (playerDist2 > radiusMax2) {
                    // TODO "were do you thing you're going?"
                    player.getPlayer().teleport(world.getLobbyLocation());
                }
            }
        }
    }
}