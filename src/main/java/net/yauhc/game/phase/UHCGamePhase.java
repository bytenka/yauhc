package net.yauhc.game.phase;

import org.bukkit.entity.Player;

import net.yauhc.game.UHCGame;
import net.yauhc.game.option.UHCGameOption;
import net.yauhc.player.UHCPlayer;
import net.yauhc.player.UHCTeam;

public abstract class UHCGamePhase {
    protected UHCGame game;
    protected UHCModuleAntiGrief antiGrief;

    public UHCGamePhase(UHCGame game) {
        if (game == null) {
            throw new IllegalArgumentException("Game cannot be null");
        }
        this.game = game;

        this.antiGrief = new UHCModuleAntiGrief(this.game, false);
    }

    // Event related to the phase itself
    public abstract void onBegin();
    public abstract void onTickSecond(int second);
    public abstract void onEnd();

    // Events about what happens on the server
    public abstract void onPlayerJoin(Player player); // A player joins the game
    public abstract void onPlayerQuit(Player player); // A player quits the game
    public abstract void onPlayerConnect(UHCPlayer player); // A known player connects
    public abstract void onPlayerDisconnect(UHCPlayer player); // A known player disconnects

    // Events triggered on player actions
    public abstract void onPlayerLocaleChange(UHCPlayer player);
    public abstract void onPlayerChangeTeam(UHCPlayer player, UHCTeam team);
    public abstract void onPlayerChangeRole(UHCPlayer player);
    
    // Events triggered on admin actions
    public abstract void onOptionUpdate(UHCGameOption option);
}