package net.yauhc.game.phase;

import org.bukkit.GameMode;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;

import net.yauhc.game.UHCGame;
import net.yauhc.main.UHCModule;

public class UHCModuleAntiGrief extends UHCModule implements Listener {

    public UHCModuleAntiGrief(UHCGame game, boolean active) {
        super(game, active);
        this.game.getPlugin().getServer().getPluginManager().registerEvents(this, this.game.getPlugin());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockPlaceEvent(BlockPlaceEvent event) {
        if (isActive() && event.getPlayer().getGameMode() != GameMode.CREATIVE) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockBreakEvent(BlockBreakEvent event) {
        if (isActive() && event.getPlayer().getGameMode() != GameMode.CREATIVE) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerBucketEmptyEvent(PlayerBucketEmptyEvent event) {
        if (isActive() && event.getPlayer().getGameMode() != GameMode.CREATIVE) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerBucketFillEvent(PlayerBucketFillEvent event) {
        if (isActive() && event.getPlayer().getGameMode() != GameMode.CREATIVE) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onEntityDamageByEntityEvent(EntityDamageByEntityEvent event) {
        if (isActive()) {
            if (event.getDamager().getType() == EntityType.PLAYER) {
                Player p = (Player) event.getDamager();
                if (p.getGameMode() != GameMode.CREATIVE) {
                    if (event.getDamager().getType() == EntityType.PLAYER) {
                        event.setCancelled(true);
                    }
                }
            }
        }
    }
}