package net.yauhc.game.phase.started;

import org.bukkit.Sound;
import org.bukkit.entity.Player;

import net.yauhc.game.UHCGame;
import net.yauhc.game.phase.UHCGamePhasePlaying;
import net.yauhc.lang.UHCLocStr;
import net.yauhc.player.UHCPlayer;

public class UHCGamePhaseStarted extends UHCGamePhasePlaying {

    private static final int INVULNERABILITY_TIME = 30;

    public UHCGamePhaseStarted(UHCGame game) {
        super(game);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onBegin() {
        this.modulePlaying.setActive(true);

        this.game.getChatManager().sendBroadcastTitle(new UHCLocStr("game", "phase", "starting", "go_title"),
        new UHCLocStr("game", "phase", "starting", "go_subtitle"), Sound.ENTITY_ENDER_DRAGON_GROWL);

        this.game.getChatManager().sendBroadcastMessage(new UHCLocStr("game", "phase", "started", "notify"), null, null);

        for (UHCPlayer player : this.game.getPlayerManager().getGamers()) {
            Player p = player.getPlayer();
            // Food
            p.setExhaustion(0);
            p.setFoodLevel(20);
            p.setSaturation(5);

            // Health
            p.setHealth(20);
            p.setAbsorptionAmount(0);

            // TODO set starting inventory
        }
    }

    @Override
    public void onTickSecond(int second) {
        if (second == 0) {
            for (UHCPlayer p : this.game.getPlayerManager().getGamers()) {
                p.getPlayer().sendMessage(new UHCLocStr("game", "phase", "started", "invulnerability_on")
                        .localize(p.getLocale()).format(INVULNERABILITY_TIME + "").toString());
                p.getPlayer().setInvulnerable(true);
            }
        }

        if (second == INVULNERABILITY_TIME) {
            for (UHCPlayer p : this.game.getPlayerManager().getGamers()) {
                p.getPlayer().sendMessage(
                    new UHCLocStr("game", "phase", "started", "invulnerability_off").localize(p.getLocale()).toString());
                p.getPlayer().setInvulnerable(false);
            }
        }
    }

    @Override
    public void onEnd() {
        // TODO Auto-generated method stub
        this.modulePlaying.setActive(false);
    }

    @Override
    public void onPlayerLocaleChange(UHCPlayer player) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPlayerChangeRole(UHCPlayer player) {
        // TODO Auto-generated method stub

    }

}