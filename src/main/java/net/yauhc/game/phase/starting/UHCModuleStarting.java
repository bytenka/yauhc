package net.yauhc.game.phase.starting;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import net.yauhc.game.UHCGame;
import net.yauhc.main.UHCModule;
import net.yauhc.player.UHCPlayer;
import net.yauhc.player.UHCPlayerManager;
import net.yauhc.player.UHCPlayer.GameRole;
import net.yauhc.world.UHCWorld;

public class UHCModuleStarting extends UHCModule implements Listener {
    private UHCGamePhaseStarting phase;

    public UHCModuleStarting(UHCGame game, boolean active, UHCGamePhaseStarting phase) {
        super(game, active);

        if (phase == null) {
            throw new IllegalArgumentException("Phase cannot be null");
        }
        this.phase = phase;

        this.game.getPlugin().getServer().getPluginManager().registerEvents(this, this.game.getPlugin());
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerMoveEvent(PlayerMoveEvent event) {
        if (isActive()) {
            UHCPlayerManager playerManager = this.game.getPlayerManager();
            UHCPlayer player = playerManager.getPlayer(event.getPlayer());
            if (player != null && player.getRole() == GameRole.PLAYER) {
                int y = UHCWorld.STARTING_PLATFORM_Y;
                if (event.getTo().getBlockY() < y) {
                    player.getPlayer().teleport(phase.getSpawnLocationOfPlayer(player).clone().add(0.5, 2, 0.5));
                }
            }
        }
    }
}