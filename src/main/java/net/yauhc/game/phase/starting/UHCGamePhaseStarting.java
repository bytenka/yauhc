package net.yauhc.game.phase.starting;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import net.yauhc.game.UHCGame;
import net.yauhc.game.option.rule.UHCGameRuleTeams;
import net.yauhc.game.phase.UHCGamePhasePlaying;
import net.yauhc.lang.UHCLocStr;
import net.yauhc.player.UHCPlayer;
import net.yauhc.player.UHCPlayerManager;
import net.yauhc.player.UHCTeam;
import net.yauhc.player.UHCTeamManager;
import net.yauhc.world.UHCWorld;

public class UHCGamePhaseStarting extends UHCGamePhasePlaying {
    private static final int DURATION = 20;

    private HashMap<UHCPlayer, Location> spawnLocations;
    private UHCModuleStarting moduleStarting;

    public UHCGamePhaseStarting(UHCGame game) {
        super(game);
        this.spawnLocations = new HashMap<>();
        this.moduleStarting = new UHCModuleStarting(this.game, false, this);
    }

    Location getSpawnLocationOfPlayer(UHCPlayer player) {
        if (player == null) {
            throw new IllegalArgumentException("Player cannot be null");
        }

        Location loc = this.spawnLocations.get(player);
        if (loc == null) {
            throw new IllegalArgumentException("Unknown player");
        }

        return loc;
    }

    @Override
    public void onBegin() {
        this.game.getChatManager().sendBroadcastMessage(new UHCLocStr("game", "phase", "starting", "notify"), null,
                Sound.ENTITY_ILLUSIONER_PREPARE_BLINDNESS);

        this.antiGrief.setActive(true);
        this.moduleStarting.setActive(true);
        this.modulePlaying.setActive(true);

        UHCPlayerManager playerManager = this.game.getPlayerManager();

        for (UHCPlayer player : playerManager.getPlayers()) {
            Player p = player.getPlayer();
            switch (player.getRole()) {
                case PLAYER:
                    p.setGameMode(GameMode.SURVIVAL);
                    p.setExp(0);
                    p.setLevel(0);
                    p.setFoodLevel(20);
                    p.setHealth(20);
                    p.setExhaustion(0);
                    p.getInventory().clear();
                    break;
                case SPECTATOR:
                    p.setGameMode(GameMode.SPECTATOR);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown player role");
            }
        }

        // Create platforms and teleport players
        UHCWorld world = this.game.getWorldManager().getWorld();
        if (this.game.getOptionManager().getRule(UHCGameRuleTeams.class).getTeamsEnabled()) {
            UHCTeamManager teamManager = this.game.getTeamManager();
            Collection<UHCTeam> teams = teamManager.getTeams();
            Collection<Location> locations = world.createStartingPlatforms(teams.size());

            Iterator<Location> it = locations.iterator();
            for (UHCTeam team : teamManager.getTeams()) {
                Location loc = it.next();
                for (UHCPlayer p : teamManager.getTeamPlayers(team)) {
                    this.spawnLocations.put(p, loc);
                    p.getPlayer().teleport(loc.clone().add(0.5, 2, 0.5));
                }
            }
        } else {
            Collection<UHCPlayer> players = playerManager.getPlayers();
            Collection<Location> locations = world.createStartingPlatforms(players.size());

            Iterator<Location> it = locations.iterator();
            for (UHCPlayer p : players) {
                Location loc = it.next();
                this.spawnLocations.put(p, loc);
                p.getPlayer().teleport(loc.clone().add(0.5, 2, 0.5));
            }
        }
    }

    @Override
    public void onTickSecond(int second) {
        if (DURATION - 5 <= second && second < DURATION) {
            this.game.getChatManager().sendBroadcastMessage(
                    new UHCLocStr("game", "phase", "starting", "countdown").format(String.valueOf(DURATION - second)), null,
                    Sound.ENTITY_EXPERIENCE_ORB_PICKUP);
        }

        if (second == DURATION) {
            this.game.nextPhase();
        }
    }

    @Override
    public void onEnd() {
        this.moduleStarting.setActive(false);
        this.modulePlaying.setActive(false);
        this.antiGrief.setActive(false);
        this.game.getWorldManager().getWorld().clearStartingPlatforms(this.spawnLocations.values());
    }

    @Override
    public void onPlayerLocaleChange(UHCPlayer player) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onPlayerChangeRole(UHCPlayer player) {
        // TODO Auto-generated method stub
    }
}