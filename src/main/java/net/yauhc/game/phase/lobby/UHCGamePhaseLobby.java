package net.yauhc.game.phase.lobby;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;

import net.yauhc.game.UHCGame;
import net.yauhc.game.option.UHCGameOption;
import net.yauhc.menu.UHCMenuAdmin;
import net.yauhc.menu.UHCMenuTeamSelectionColor;
import net.yauhc.game.phase.UHCGamePhase;
import net.yauhc.player.UHCPlayer;
import net.yauhc.player.UHCPlayerManager;
import net.yauhc.player.UHCTeam;
import net.yauhc.player.UHCTeamManager;

public class UHCGamePhaseLobby extends UHCGamePhase {
    private UHCModuleLobby coolLobbyEffect;

    public UHCGamePhaseLobby(UHCGame game) {
        super(game);

        this.coolLobbyEffect = new UHCModuleLobby(this.game, false);
    }

    @Override
    public void onBegin() {
        this.coolLobbyEffect.setActive(true);
        this.antiGrief.setActive(true);
        this.game.getWorldManager().getWorld().createLobbyPlatform();

        UHCPlayerManager playerManager = this.game.getPlayerManager();
        for (UHCPlayer p : playerManager.getPlayers()) {
            // Re-trigger events to reset already present players
            onPlayerDisconnect(p);
            onPlayerConnect(p);
        }
    }

    @Override
    public void onTickSecond(int second) {
        // Nothing to do here, we just wait for the game to start
    }

    @Override
    public void onEnd() {
        this.coolLobbyEffect.setActive(false);
        this.antiGrief.setActive(false);
    }

    @Override
    public void onPlayerJoin(Player player) {
        // A new player connects. Because onPlayerQuit delete every player from the
        // player manager, the new player should always be unknown for this phase
        UHCPlayerManager playerManager = this.game.getPlayerManager();

        UHCPlayer uhcPlayer = playerManager.getPlayer(player);
        if (uhcPlayer != null) {
            throw new IllegalStateException("Player should not exist");
        }
        uhcPlayer = playerManager.addPlayer(player, UHCPlayer.GameRole.PLAYER);
        onPlayerConnect(uhcPlayer);
    }

    @Override
    public void onPlayerQuit(Player player) {
        // We don't need to keep any info on the player, so delete them from the manager
        UHCPlayerManager playerManager = this.game.getPlayerManager();

        UHCPlayer uhcPlayer = playerManager.getPlayer(player);
        if (uhcPlayer == null) {
            throw new IllegalStateException("Player should exist");
        }
        onPlayerDisconnect(uhcPlayer);
        playerManager.removePlayer(player);
    }

    @Override
    public void onPlayerConnect(UHCPlayer player) {
        // Initialize player state
        this.game.getPlayerManager().setPlayerRole(player, UHCPlayer.GameRole.PLAYER);

        setPlayerInventory(player);

        Player p = player.getPlayer();
        p.setGameMode(GameMode.SURVIVAL);
        p.setFoodLevel(20);
        p.setHealth(20);
        p.setExp(0);
        p.setLevel(0);
        p.setInvulnerable(true);

        p.teleport(this.game.getWorldManager().getWorld().getLobbyLocation());
        // Prevent clipping though the lobby block + center on block
        p.teleport(p.getLocation().add(0.5, 2, 0.5));
    }

    @Override
    public void onPlayerDisconnect(UHCPlayer player) {
        // If player was in a team, make them quit
        UHCTeamManager teamManager = this.game.getTeamManager();

        if (player.getTeam() != null)
            teamManager.setPlayerTeam(player, null);

        if (player.getMoleTeam() != null)
            teamManager.setPlayerMoleTeam(player, null);

        player.setRevealed(false);
    }

    @Override
    public void onPlayerChangeTeam(UHCPlayer player, UHCTeam team) {
        // In case some items are dependant or the player's team (like a banner color)
        setPlayerInventory(player);
    }

    @Override
    public void onPlayerLocaleChange(UHCPlayer player) {
        // We need to give new items with new locale
        setPlayerInventory(player);
    }

    @Override
    public void onPlayerChangeRole(UHCPlayer player) {
        setPlayerInventory(player);
    }

    @Override
    public void onOptionUpdate(UHCGameOption option) {
        // Updated option can mean that teams are now enabled or disabled, so we need to
        // give or take back any team related items
        for (UHCPlayer p : this.game.getPlayerManager().getPlayers()) {
            setPlayerInventory(p);
        }
    }

    private void setPlayerInventory(UHCPlayer player) {
        Inventory inv = Bukkit.createInventory(player.getPlayer(), InventoryType.PLAYER);

        // Give the team selection item if teams are enabled
        if (this.game.getTeamManager().getTeamEnabled()) {
            inv.setItem(8, this.game.getMenuManager().getMenuSpawnItem(UHCMenuTeamSelectionColor.class, player));
        }

        // Give the control panel item to OP players
        if (player.getPlayer().isOp()) {
            inv.setItem(1, this.game.getMenuManager().getMenuSpawnItem(UHCMenuAdmin.class, player));
        }

        player.getPlayer().getInventory().setContents(inv.getContents());
    }
}