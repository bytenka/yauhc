package net.yauhc.game.phase.lobby;

import java.util.Arrays;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import net.yauhc.player.UHCTeam;
import net.yauhc.game.UHCGame;
import net.yauhc.main.UHCModule;
import net.yauhc.player.UHCPlayer;
import net.yauhc.player.UHCPlayerManager;
import net.yauhc.world.UHCWorld;

public class UHCModuleLobby extends UHCModule implements Listener {

    public UHCModuleLobby(UHCGame game, boolean active) {
        super(game, active);
        this.game.getPlugin().getServer().getPluginManager().registerEvents(this, this.game.getPlugin());
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerMoveEvent(PlayerMoveEvent event) {
        if (isActive()) {
            UHCPlayerManager playerManager = this.game.getPlayerManager();
            UHCPlayer player = playerManager.getPlayer(event.getPlayer());
            if (player != null && player.getTeam() != null) {
                UHCWorld world = this.game.getWorldManager().getWorld();
                Material[] floor = world.getFloorMaterials();

                for (int i = 1; i <= 2; i++) {
                    Location loc = event.getTo().clone().add(0, -i, 0);
                    Block block = world.getWorld().getBlockAt(loc);
                    if (Arrays.asList(floor).contains(block.getType())) {
                        block.setType(UHCTeam.colorToStainedGlass(player.getTeam().getColor()));
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onFoodLevelChangeEvent(FoodLevelChangeEvent event) {
        if (isActive()) {
            UHCPlayerManager playerManager = this.game.getPlayerManager();
            if (event.getEntityType() == EntityType.PLAYER) {
                UHCPlayer player = playerManager.getPlayer((Player)event.getEntity());
                if (player != null) {
                    if (event.getFoodLevel() < 20) {
                        event.setCancelled(true);
                        player.getPlayer().setFoodLevel(20);
                    }
                }
            }

        }
    }
}