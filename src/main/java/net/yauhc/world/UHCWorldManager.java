package net.yauhc.world;

import net.yauhc.game.UHCGame;

public class UHCWorldManager {
    private UHCGame game;
    private UHCWorld world;

    public UHCWorldManager(UHCGame game) {
        if (game == null) {
            throw new IllegalArgumentException("Game cannot be null");
        }
        this.game = game;

        this.world = new UHCWorld(this.game);
    }

    public UHCWorld getWorld() {
        return this.world;
    }
}