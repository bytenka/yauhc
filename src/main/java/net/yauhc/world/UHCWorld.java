package net.yauhc.world;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;

import net.yauhc.game.UHCGame;
import net.yauhc.game.option.rule.UHCGameRuleWorldBorder;

public class UHCWorld {
    private UHCGame game;
    private World world;

    public static final int LOBBY_PLATFORM_SIZE = 25;
    public static final int LOBBY_PLATFORM_Y = 190;

    public static final int STARTING_PLATFORM_SIZE = 5;
    public static final int STARTING_PLATFORM_Y = 180;
    private static final int STARTING_PLATFORM_WORLD_MARGIN = 50;
    private static final int STARTING_PLATFORM_DIST_FROM_OTHERS = 100 + STARTING_PLATFORM_SIZE * 2;

    private Material[] floorMaterials = { Material.RED_STAINED_GLASS, Material.PINK_STAINED_GLASS,
            Material.ORANGE_STAINED_GLASS, Material.YELLOW_STAINED_GLASS, Material.BLUE_STAINED_GLASS,
            Material.LIGHT_BLUE_STAINED_GLASS, Material.GREEN_STAINED_GLASS, Material.LIME_STAINED_GLASS,
            Material.PURPLE_STAINED_GLASS, Material.MAGENTA_STAINED_GLASS, };

    public UHCWorld(UHCGame game) {
        if (game == null) {
            throw new IllegalArgumentException("Game cannot be null");
        }
        this.game = game;

        this.world = createWorld();
    }

    public Location getLobbyLocation() {
        return new Location(this.world, 0, LOBBY_PLATFORM_Y, 0);
    }

    public void createLobbyPlatform() {
        // Make sure the area is clear
        for (int x = -LOBBY_PLATFORM_SIZE; x <= LOBBY_PLATFORM_SIZE; x++) {
            for (int z = -LOBBY_PLATFORM_SIZE; z <= LOBBY_PLATFORM_SIZE; z++) {
                for (int y = 0; y <= 3; y++) {
                    this.world.getBlockAt(x, LOBBY_PLATFORM_Y + y, z).setType(Material.AIR);
                }
            }
        }

        // Floor
        Random r = new Random();
        for (int x = -LOBBY_PLATFORM_SIZE + 1; x <= LOBBY_PLATFORM_SIZE - 1; x++) {
            for (int z = -LOBBY_PLATFORM_SIZE + 1; z <= LOBBY_PLATFORM_SIZE - 1; z++) {
                this.world.getBlockAt(x, LOBBY_PLATFORM_Y, z)
                        .setType(this.floorMaterials[r.nextInt(this.floorMaterials.length)]);
            }
        }

        // Walls
        for (int i = -LOBBY_PLATFORM_SIZE; i <= LOBBY_PLATFORM_SIZE; i++) {
            for (int y = 1; y <= 3; y++) {
                this.world.getBlockAt(LOBBY_PLATFORM_SIZE, LOBBY_PLATFORM_Y + y, i).setType(Material.BARRIER);
                this.world.getBlockAt(-LOBBY_PLATFORM_SIZE, LOBBY_PLATFORM_Y + y, i).setType(Material.BARRIER);
                this.world.getBlockAt(i, LOBBY_PLATFORM_Y + y, LOBBY_PLATFORM_SIZE).setType(Material.BARRIER);
                this.world.getBlockAt(i, LOBBY_PLATFORM_Y + y, -LOBBY_PLATFORM_SIZE).setType(Material.BARRIER);
            }
        }
    }

    public Collection<Location> createStartingPlatforms(int howMany) {
        if (howMany < 0) {
            throw new IllegalArgumentException("Platform count cannot be negative");
        }

        Random r = new Random();
        UHCGameRuleWorldBorder wb = this.game.getOptionManager().getRule(UHCGameRuleWorldBorder.class);
        ArrayList<Location> locations = new ArrayList<>(howMany);

        for (int i = 0; i < howMany; i++) {
            int distFromOtherPlatforms = STARTING_PLATFORM_DIST_FROM_OTHERS;
            Location loc;
            boolean valid;
            do {
                valid = true;
                int radius = (wb.getStartSize() / 2) - (STARTING_PLATFORM_SIZE * 2)
                        - (STARTING_PLATFORM_WORLD_MARGIN * 2);

                // Polar coordinates
                double dist = r.nextDouble() * radius + STARTING_PLATFORM_WORLD_MARGIN;
                double angle = r.nextDouble() * Math.PI * 2;

                // To cartesian
                int x = (int) (dist * Math.cos(angle));
                int z = (int) (dist * Math.sin(angle));

                loc = new Location(this.world, x, STARTING_PLATFORM_Y, z);

                for (Location l : locations) {
                    if (loc.distance(l) < distFromOtherPlatforms) {
                        valid = false;
                        // Decrease minimum distance each time we fail, so this will not loop forever
                        distFromOtherPlatforms--;
                        break;
                    }
                }
            } while (!valid);

            locations.add(loc);
        }

        for (Location loc : locations) {
            for (int x = -STARTING_PLATFORM_SIZE + 1; x <= STARTING_PLATFORM_SIZE - 1; x++) {
                for (int z = -STARTING_PLATFORM_SIZE + 1; z <= STARTING_PLATFORM_SIZE - 1; z++) {
                    this.world.getBlockAt(loc.getBlockX() + x, loc.getBlockY(), loc.getBlockZ() + z)
                            .setType(Material.WHITE_STAINED_GLASS);
                }
            }
        }

        return locations;
    }

    public void clearStartingPlatforms(Collection<Location> locations) {
        if (locations == null) {
            throw new IllegalArgumentException("Locations cannot be null");
        }

        for (Location loc : locations) {
            for (int x = -STARTING_PLATFORM_SIZE + 1; x <= STARTING_PLATFORM_SIZE - 1; x++) {
                for (int z = -STARTING_PLATFORM_SIZE + 1; z <= STARTING_PLATFORM_SIZE - 1; z++) {
                    this.world.getBlockAt(loc.getBlockX() + x, loc.getBlockY(), loc.getBlockZ() + z)
                            .setType(Material.AIR);
                }
            }
        }
    }

    public World getWorld() {
        return this.world;
    }

    public Material[] getFloorMaterials() {
        return this.floorMaterials;
    }

    private World createWorld() {
        // TODO create world
        World w = this.game.getPlugin().getServer().getWorld("world");
        return w;
    }
}