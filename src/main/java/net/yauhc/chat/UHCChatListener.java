package net.yauhc.chat;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import net.yauhc.game.UHCGame;
import net.yauhc.player.UHCPlayer;

public class UHCChatListener implements Listener {

    private UHCGame game;

    public UHCChatListener(UHCGame game) {
        if (game == null) {
            throw new IllegalArgumentException("Game cannot be null");
        }
        this.game = game;
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onAsyncPlayerChatEvent(AsyncPlayerChatEvent event) {
        UHCPlayer player = this.game.getPlayerManager().getPlayer(event.getPlayer());
        String message = event.getMessage();

        if (player != null && !message.isEmpty()) {
            event.setCancelled(true);
            this.game.getChatManager().sendPlayerMessage(player, message);
        }
    }
}