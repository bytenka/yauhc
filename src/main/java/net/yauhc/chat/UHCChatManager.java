package net.yauhc.chat;

import java.util.Collection;

import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;

import net.yauhc.game.UHCGame;
import net.yauhc.lang.UHCLocStr;
import net.yauhc.lang.UHCLocale;
import net.yauhc.player.UHCPlayer;
import net.yauhc.player.UHCPlayerManager;
import net.yauhc.player.UHCTeam;
import net.yauhc.player.UHCTeamManager;
import net.yauhc.player.UHCPlayer.GameRole;

public class UHCChatManager {
    public static final char TEAM_CHAT_PREFIX = '!';

    private UHCGame game;
    private boolean teamChat;

    public UHCChatManager(UHCGame game) {
        if (game == null) {
            throw new IllegalArgumentException("Game cannot be null");
        }
        this.game = game;
        this.game.getPlugin().getServer().getPluginManager().registerEvents(new UHCChatListener(this.game),
                this.game.getPlugin());

        this.teamChat = true;
    }

    public boolean isTeamChatActive() {
        return this.teamChat;
    }

    public void setTeamChatActive(boolean active) {
        this.teamChat = active;
    }

    /**
     * Send a message in the chat as "player". Auto detect appropriate channel
     * considering player's team, message content, game context etc...
     * 
     * @param player  The player that sends the message
     * @param message What the player says
     */
    public void sendPlayerMessage(UHCPlayer player, String message) {
        Channel channel = this.teamChat ? Channel.TEAM : Channel.ALL;

        String chatPrefix = String.valueOf(TEAM_CHAT_PREFIX);
        if (message.startsWith(chatPrefix)) {
            channel = Channel.ALL;
            message = message.substring(chatPrefix.length());
        }

        if (player.getTeam() == null) {
            channel = Channel.ALL;
        }

        if (player.getRole() == GameRole.SPECTATOR) {
            channel = Channel.SPECTATORS;
        }

        sendPlayerMessage(player, channel, message);
    }

    /**
     * Send a message in the chat as "player". "channel" must make sense in the
     * player's context
     * 
     * @param player  The player that sends the message
     * @param channel The channel to send the message in
     * @param message What the player says
     */
    public void sendPlayerMessage(UHCPlayer player, Channel channel, String message) {
        if (channel == Channel.TEAM && player.getTeam() == null) {
            throw new IllegalArgumentException("Channel is TEAM but player is not in a team");
        }

        if (channel == Channel.MOLE_TEAM && player.getMoleTeam() == null) {
            throw new IllegalArgumentException("Channel is MOLE_TEAM but player is not in a mole team");
        }

        if (player.getRole() == GameRole.SPECTATOR && channel != Channel.SPECTATORS) {
            throw new IllegalArgumentException("Spectators can only send messages in the SPECTATORS channel");
        }

        if (message.isEmpty()) {
            return;
        }

        UHCPlayerManager playerManager = this.game.getPlayerManager();
        UHCTeamManager teamManager = this.game.getTeamManager();
        UHCTeam playerTeam = player.getTeam();
        UHCTeam playerMoleTeam = player.getMoleTeam();

        Collection<UHCPlayer> recipients;
        String channelColorS;
        switch (channel) {
            case ALL:
                channelColorS = ChatColor.AQUA + "" + ChatColor.BOLD;
                recipients = playerManager.getPlayers();
                break;
            case TEAM:
                channelColorS = ChatColor.AQUA + "";
                recipients = teamManager.getTeamPlayers(playerTeam);
                break;
            case MOLE_TEAM:
                channelColorS = ChatColor.DARK_AQUA + "";
                recipients = teamManager.getTeamPlayers(playerMoleTeam);
                break;
            case SPECTATORS:
                channelColorS = ChatColor.GRAY + "";
                recipients = playerManager.getSpectators();
                break;
            default:
                throw new IllegalArgumentException("Unknown channel");
        }
        String teamColorS = (playerTeam == null ? ChatColor.WHITE : UHCTeam.colorToChatColor(playerTeam.getColor()))
                + "";
        String playerColorS = playerMoleTeam == null ? teamColorS : channelColorS;
        String messageColorS = ChatColor.WHITE + "";

        String playerS = String.format("%s%s%s", playerColorS, player.getPlayer().getName(), ChatColor.RESET);
        if (playerTeam != null) {
            playerS = String.format("%s%s%s %s", teamColorS, UHCTeam.prefixToString(playerTeam.getPrefix()),
                    ChatColor.RESET, playerS);
        }
        for (UHCPlayer p : recipients) {
            UHCLocale tr = p.getLocale();
            String channelS = String.format("%s[%s]%s", channelColorS,
            new UHCLocStr("chat", "channel", UHCChatManager.channelToString(channel)).localize(tr), ChatColor.RESET);
            String messageS = message;
            if (messageS.contains(p.getPlayer().getName())) {
                messageS = messageS.replace(p.getPlayer().getName(),
                        ChatColor.GOLD + "" + ChatColor.BOLD + p.getPlayer().getName() + messageColorS);
                p.getPlayer().playSound(p.getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP,
                        SoundCategory.MASTER, 1.0f, 1.0f);
            }

            p.getPlayer().sendMessage(
                    String.format("%s %s » %s%s%s", channelS, playerS, messageColorS, messageS, ChatColor.RESET));
        }
    }

    /**
     * Send a message to all players, optionally with a notification sound
     * 
     * @param message   The message to send. Eventual formatting must be done before
     *                  calling this function
     * @param sender    The part of the code that sends the message, like a specific
     *                  scenario, or null to use default plugin name
     * @param pingSound The sound to play, or null for no sound
     */
    public void sendBroadcastMessage(UHCLocStr message, UHCLocStr sender, Sound pingSound) {
        if (message == null) {
            throw new IllegalArgumentException("Message cannot be null");
        }

        UHCLocStr actualSender = sender != null ? sender : new UHCLocStr("game", "display_name");

        for (UHCPlayer p : this.game.getPlayerManager().getPlayers()) {
            String str = String.format("%s[%s%s%s]%s %s%s%s", ChatColor.GOLD + "" + ChatColor.BOLD,
                    ChatColor.RED + "" + ChatColor.BOLD, actualSender.localize(p.getLocale()),
                    ChatColor.RESET + "" + ChatColor.GOLD + ChatColor.BOLD, ChatColor.RESET, ChatColor.WHITE,
                    message.localize(p.getLocale()), ChatColor.RESET);

            if (pingSound != null) {
                p.getPlayer().playSound(p.getPlayer().getLocation(), pingSound, SoundCategory.MASTER, 1.0f, 1.0f);
            }

            p.getPlayer().sendMessage(str);
        }
    }

    /**
     * Send a message to all players via the title mechanism, optionally with a
     * notification sound
     * 
     * @param title     The title to send, or null
     * @param subtitle  The subtitle to send, or null
     * @param pingSound The sound to play, or null for no sound
     */
    public void sendBroadcastTitle(UHCLocStr title, UHCLocStr subtitle, Sound pingSound) {
        for (UHCPlayer p : this.game.getPlayerManager().getPlayers()) {
            String actualTitle = title != null ? title.localize(p.getLocale()).toString() : "";
            String actualSubtitle = subtitle != null ? subtitle.localize(p.getLocale()).toString() : "";

            if (pingSound != null) {
                p.getPlayer().playSound(p.getPlayer().getLocation(), pingSound, SoundCategory.MASTER, 1.0f, 1.0f);
            }

            p.getPlayer().sendTitle(actualTitle, actualSubtitle, 10, 70, 20);
        }
    }

    public enum Channel {
        ALL, TEAM, MOLE_TEAM, SPECTATORS
    }

    private static String channelToString(Channel channel) {
        switch (channel) {
            case ALL:
                return "all";
            case TEAM:
                return "team";
            case MOLE_TEAM:
                return "mole_team";
            case SPECTATORS:
                return "spectators";
            default:
                throw new IllegalArgumentException("Unknown channel");
        }
    }
}