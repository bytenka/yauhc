package net.yauhc.main;

import net.yauhc.game.UHCGame;

public class UHCModule {
    protected UHCGame game;
    protected boolean active = false;

    public UHCModule(UHCGame game, boolean active) {
        if (game == null) {
            throw new IllegalArgumentException("Game cannot be null");
        }
        this.game = game;
        this.active = active;
    }

    public boolean isActive() {
        return this.active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}