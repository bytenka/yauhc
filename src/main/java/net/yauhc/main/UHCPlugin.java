package net.yauhc.main;

import org.bukkit.plugin.java.JavaPlugin;

import net.yauhc.game.UHCGame;
import net.yauhc.command.UHCCommandManager;

public class UHCPlugin extends JavaPlugin {
    private UHCCommandManager commandManager;
    private UHCGame game;

    @Override
    public void onLoad() {
    }

    @Override
    public void onEnable() {
        this.commandManager = new UHCCommandManager(this);
        this.game = new UHCGame(this);
    }

    @Override
    public void onDisable() {

    }

    public UHCGame getGame() {
        return this.game;
    }

    public UHCCommandManager getCommandManager() {
        return this.commandManager;
    }
}