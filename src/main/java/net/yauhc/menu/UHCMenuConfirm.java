package net.yauhc.menu;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.meta.ItemMeta;

import net.yauhc.game.UHCGame;
import net.yauhc.lang.UHCLocStr;
import net.yauhc.lang.UHCLocale;
import net.yauhc.player.UHCPlayer;

public class UHCMenuConfirm extends UHCMenu {

    public static final String ON_ACCEPT = "menu_confirm_on_accept";
    public static final String ON_REJECT = "menu_confirm_on_reject";

    public UHCMenuConfirm(UHCGame game, UHCMenu previous) {
        super(game, previous);

        if (previous == null) {
            throw new IllegalArgumentException("Previous menu cannot be null for this menu");
        }
    }

    @Override
    AccessRight getAccessRight() {
        return AccessRight.PLAYER;
    }

    @Override
    UHCMenuItem getSpawnItem(UHCPlayer player) {
        return null;
    }

    @Override
    protected void openPrimary(UHCPlayer player, String[] args) {
        if (args.length < 1) {
            throw new IllegalArgumentException("Missing action description");
        }

        String[] newArgsOnAccept = new String[args.length];
        System.arraycopy(args, 0, newArgsOnAccept, 0, args.length);
        newArgsOnAccept[newArgsOnAccept.length - 1] = ON_ACCEPT;

        String[] newArgsOnReject = new String[args.length];
        System.arraycopy(args, 0, newArgsOnReject, 0, args.length);
        newArgsOnReject[newArgsOnReject.length - 1] = ON_REJECT;

        UHCLocale tr = player.getLocale();

        Inventory inv = Bukkit.createInventory(player.getPlayer(), InventoryType.DISPENSER,
                new UHCLocStr("menu", "confirm", "title").localize(tr).toString());

        // Override color
        this.decoration.setType(Material.ORANGE_STAINED_GLASS_PANE);

        UHCMenuItem itemActionDesc = new UHCMenuItem(this.game, Material.PAPER, "");
        ItemMeta itemActionDescMeta = itemActionDesc.getItemMeta();
        itemActionDescMeta.setDisplayName(
                new UHCLocStr("menu", "confirm", "item", "description", "name").localize(tr).toString());
        ArrayList<String> itemActionLore = new ArrayList<>();
        itemActionLore.add(ChatColor.AQUA + args[args.length - 1]);
        itemActionDescMeta.setLore(itemActionLore);
        itemActionDesc.setItemMeta(itemActionDescMeta);

        UHCMenuItem itemOnAccept = new UHCMenuItem(this.game, Material.SLIME_BALL,
                this.previous.getURL(newArgsOnAccept));
        ItemMeta itemOnAcceptMeta = itemOnAccept.getItemMeta();
        itemOnAcceptMeta
                .setDisplayName(new UHCLocStr("menu", "confirm", "item", "accept", "name").localize(tr).toString());
        ArrayList<String> itemOnAcceptLore = new ArrayList<>();
        itemOnAcceptLore.add(new UHCLocStr("menu", "confirm", "item", "accept", "lore").localize(tr).toString());
        itemOnAcceptMeta.setLore(itemOnAcceptLore);
        itemOnAccept.setItemMeta(itemOnAcceptMeta);

        UHCMenuItem itemOnReject = new UHCMenuItem(this.game, Material.BARRIER, this.previous.getURL(newArgsOnReject));
        ItemMeta itemOnRejectMeta = itemOnReject.getItemMeta();
        itemOnRejectMeta
                .setDisplayName(new UHCLocStr("menu", "confirm", "item", "reject", "name").localize(tr).toString());
        ArrayList<String> itemOnRejectLore = new ArrayList<>();
        itemOnRejectLore.add(new UHCLocStr("menu", "confirm", "item", "reject", "lore").localize(tr).toString());
        itemOnRejectMeta.setLore(itemOnRejectLore);
        itemOnReject.setItemMeta(itemOnRejectMeta);

        int[] decorationPos = { 0, 1, 2, 6, 7, 8 };
        for (int pos : decorationPos) {
            inv.setItem(pos, this.decoration);
        }

        inv.setItem(3, itemOnAccept);
        inv.setItem(4, itemActionDesc);
        inv.setItem(5, itemOnReject);

        player.getPlayer().openInventory(inv);
    }

    @Override
    protected void openSecondary(UHCPlayer player, String[] args) {
        openSecondary(player, args);
    }

}