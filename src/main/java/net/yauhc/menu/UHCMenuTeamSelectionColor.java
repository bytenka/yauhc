package net.yauhc.menu;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.meta.ItemMeta;

import net.yauhc.game.UHCGame;
import net.yauhc.lang.UHCLocStr;
import net.yauhc.lang.UHCLocale;
import net.yauhc.player.UHCPlayer;
import net.yauhc.player.UHCTeam;
import net.yauhc.player.UHCTeam.Prefix;

public class UHCMenuTeamSelectionColor extends UHCMenu {

    public UHCMenuTeamSelectionColor(UHCGame game, UHCMenu previous) {
        super(game, previous);
    }

    @Override
    AccessRight getAccessRight() {
        return AccessRight.PLAYER;
    }

    @Override
    UHCMenuItem getSpawnItem(UHCPlayer player) {
        UHCLocale tr = player.getLocale();

        Material bannerColor = player.getTeam() != null ? UHCTeam.colorToBanner(player.getTeam().getColor())
                : Material.WHITE_BANNER;

        UHCMenuItem item = new UHCMenuItem(game, bannerColor, getURL());
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(
                String.format("%s%s ∙ (%s)%s", new UHCLocStr("menu", "team", "selection", "spawn_item").localize(tr),
                        ChatColor.GRAY, new UHCLocStr("menu", "action", "right_click").localize(tr), ChatColor.RESET));
        item.setItemMeta(meta);

        return item;
    }

    @Override
    protected void openPrimary(UHCPlayer player, String[] args) {
        if (player == null) {
            throw new IllegalArgumentException("Player cannot be null");
        }

        UHCTeam.Prefix prefix = Prefix.SPADE;

        if (args.length > 0) {
            switch (args[0]) {
                case "leave_team": {
                    this.game.getTeamManager().setPlayerTeam(player, null);
                    player.getPlayer().closeInventory();
                    return;
                }

                default:
                    throw new IllegalArgumentException("Unknown action " + args[0]);
            }
        }

        Inventory inventory = createInventory(player, prefix);
        player.getPlayer().openInventory(inventory);
    }

    @Override
    protected void openSecondary(UHCPlayer player, String[] args) {
        openPrimary(player, args);
    }

    private Inventory createInventory(UHCPlayer player, UHCTeam.Prefix prefix) {
        UHCLocale tr = player.getLocale();

        Inventory inventory = Bukkit.createInventory(player.getPlayer(), 36,
                new UHCLocStr("menu", "team", "selection", "color", "title").localize(tr).toString());

        inventory.setItem(11, createTeamBanner(UHCTeam.Color.DARK_RED, tr));
        inventory.setItem(12, createTeamBanner(UHCTeam.Color.ORANGE, tr));
        inventory.setItem(13, createTeamBanner(UHCTeam.Color.YELLOW, tr));
        inventory.setItem(14, createTeamBanner(UHCTeam.Color.GREEN, tr));
        inventory.setItem(15, createTeamBanner(UHCTeam.Color.DARK_GREEN, tr));

        inventory.setItem(20, createTeamBanner(UHCTeam.Color.BLUE, tr));
        inventory.setItem(21, createTeamBanner(UHCTeam.Color.DARK_BLUE, tr));
        inventory.setItem(22, createTeamBanner(UHCTeam.Color.PURPLE, tr));
        inventory.setItem(23, createTeamBanner(UHCTeam.Color.MAGENTA, tr));
        inventory.setItem(24, createTeamBanner(UHCTeam.Color.RED, tr));

        UHCMenuItem leaveTeam = new UHCMenuItem(this.game, Material.BARRIER, getURL("leave_team"));
        ItemMeta leaveTeamMeta = leaveTeam.getItemMeta();
        leaveTeamMeta
                .setDisplayName(new UHCLocStr("menu", "team", "selection", "leave", "name").localize(tr).toString());
        ArrayList<String> leaveTeamLore = new ArrayList<>();
        leaveTeamLore.add(new UHCLocStr("menu", "team", "selection", "leave", "lore").localize(tr).toString());
        leaveTeamMeta.setLore(leaveTeamLore);
        leaveTeam.setItemMeta(leaveTeamMeta);
        inventory.setItem(31, leaveTeam);

        // Custom decoration if in team
        UHCTeam playerTeam = player.getTeam();
        if (playerTeam != null) {
            this.decoration.setType(UHCTeam.colorToStainedGlassPane(playerTeam.getColor()));
            ItemMeta decorationMeta = this.decoration.getItemMeta();
            decorationMeta.setDisplayName(
                    UHCTeam.colorToChatColor(playerTeam.getColor()) + UHCTeam.prefixToString(playerTeam.getPrefix()));
            this.decoration.setItemMeta(decorationMeta);
        }
        int[] decorationPos = { 0, 1, 7, 8, 9, 17, 18, 26, 27, 28, 34, 35 };
        for (int i : decorationPos) {
            inventory.setItem(i, this.decoration);
        }

        return inventory;
    }

    private UHCMenuItem createTeamBanner(UHCTeam.Color color, UHCLocale tr) {
        String action = new UHCMenuTeamSelectionPrefix(this.game, this).getURL(UHCTeam.colorToString(color));

        UHCMenuItem item = new UHCMenuItem(this.game, UHCTeam.colorToBanner(color), action);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(String.format("%s%s%s", UHCTeam.colorToChatColor(color),
                new UHCLocStr("menu", "team", "selection", "color", "select", "name").localize(tr)
                        .format(new UHCLocStr("game", "team", "color", UHCTeam.colorToString(color)).localize(tr)),
                ChatColor.RESET));

        ArrayList<String> lore = new ArrayList<>();
        // ▶ = U+25B6
        lore.add(ChatColor.GRAY + "▶ "
                + new UHCLocStr("menu", "team", "selection", "color", "select", "lore").localize(tr));
        meta.setLore(lore);

        item.setItemMeta(meta);
        return item;
    }
}