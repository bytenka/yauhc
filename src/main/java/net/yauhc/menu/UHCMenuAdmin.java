package net.yauhc.menu;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.meta.ItemMeta;

import net.yauhc.game.UHCGame;
import net.yauhc.player.UHCPlayer;

public class UHCMenuAdmin extends UHCMenu {

    public UHCMenuAdmin(UHCGame game, UHCMenu previous) {
        super(game, previous);
    }

    @Override
    AccessRight getAccessRight() {
        return AccessRight.ADMIN;
    }

    @Override
    UHCMenuItem getSpawnItem(UHCPlayer player) {
        UHCMenuItem item = new UHCMenuItem(this.game, Material.COMMAND_BLOCK, getURL());
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName("Test");
        item.setItemMeta(meta);
        return item;
    }

    @Override
    protected void openPrimary(UHCPlayer player, String[] args) {
        Inventory inv = Bukkit.createInventory(player.getPlayer(), InventoryType.CHEST);

        String url = new UHCMenuConfirm(this.game, this).getURL("Test");

        UHCMenuItem item = new UHCMenuItem(this.game, Material.SAND, url);

        if (args.length > 0) {
            switch (args[args.length - 1]) {
                case UHCMenuConfirm.ON_ACCEPT:
                    player.getPlayer().closeInventory();
                    break;
                default:
                    break;
            }
        }

        inv.setItem(0, item);
        player.getPlayer().openInventory(inv);
    }

    @Override
    protected void openSecondary(UHCPlayer player, String[] args) {
        openPrimary(player, args);
    }
}