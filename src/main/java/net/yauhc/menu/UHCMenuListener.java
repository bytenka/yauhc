package net.yauhc.menu;

import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

import net.yauhc.game.UHCGame;
import net.yauhc.menu.UHCMenu.ActionType;
import net.yauhc.player.UHCPlayer;

public class UHCMenuListener implements Listener {
    private UHCGame game;

    public UHCMenuListener(UHCGame game) {
        if (game == null) {
            throw new IllegalArgumentException("Game cannot be null");
        }
        this.game = game;
    }

    private String getMenuAction(ItemStack item) {
        if (item == null)
            return null;

        ItemMeta meta = item.getItemMeta();
        if (meta == null)
            return null;

        PersistentDataContainer container = meta.getPersistentDataContainer();
        NamespacedKey key = new NamespacedKey(this.game.getPlugin(), UHCMenuItem.PERSISTENCE_DATA_KEY);

        return container.get(key, PersistentDataType.STRING);
    }

    private void handle(ItemStack item, Player player, String url, UHCMenu.ActionType actionType) {
        if (item == null) {
            throw new IllegalArgumentException("Item cannot be null");
        }

        if (player == null) {
            throw new IllegalArgumentException("Player cannot be null");
        }

        if (url == null) {
            throw new IllegalArgumentException("URL cannot be null");
        }

        UHCPlayer uhcPlayer = this.game.getPlayerManager().getPlayer(player);
        if (uhcPlayer == null) {
            throw new IllegalArgumentException("Programming error: player is unknown to player manager");
        }

        UHCMenuManager manager = this.game.getMenuManager();
        manager.openMenu(uhcPlayer, url, actionType);
    }

    // Clicking an item in the inventory
    @EventHandler
    public void onInventoryClickEvent(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        ItemStack item = event.getCurrentItem();
        ActionType actionType = event.isRightClick() ? ActionType.SECONDARY : ActionType.PRIMARY;

        String url = getMenuAction(item);
        if (url != null) {
            event.setCancelled(true);
            handle(item, player, url, actionType);
        }
    }

    // Clicking while holding an item
    @EventHandler
    public void onPlayerInteractEvent(PlayerInteractEvent event) {
        // Prevent fireing twice (once for each hand)
        if (event.getHand() == EquipmentSlot.OFF_HAND)
            return;

        // Only fire on right click for this one
        if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            Player player = event.getPlayer();
            ItemStack item = event.getPlayer().getInventory().getItemInMainHand();
            ActionType actionType = ActionType.PRIMARY;

            String url = getMenuAction(item);
            if (url != null) {
                event.setCancelled(true);
                handle(item, player, url, actionType);
            }
        }
    }

    // Prevent item swapping to keep the item in the same slot
    @EventHandler
    public void onPlayerSwapHandItemsEvent(PlayerSwapHandItemsEvent event) {
        Player player = event.getPlayer();
        ItemStack item = event.getOffHandItem();
        ActionType actionType = ActionType.PRIMARY;

        String url = getMenuAction(item);
        if (url != null) {
            event.setCancelled(true);
            handle(item, player, url, actionType);
        }
    }

    // Prevent item dropping
    @EventHandler
    public void onPlayerDropItemEvent(PlayerDropItemEvent event) {
        Player player = event.getPlayer();
        ItemStack item = event.getItemDrop().getItemStack();
        ActionType actionType = ActionType.PRIMARY;

        String url = getMenuAction(item);
        if (url != null) {
            event.setCancelled(true);
            handle(item, player, url, actionType);
        }
    }

    // Prevent item dropping on death
    @EventHandler
    private void onItemSpawnEvent(ItemSpawnEvent event) {
        ItemStack item = event.getEntity().getItemStack();

        String url = getMenuAction(item);
        if (url != null) {
            event.setCancelled(true);
        }
    }
}