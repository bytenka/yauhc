package net.yauhc.menu;

import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;

import net.yauhc.game.UHCGame;

public class UHCMenuItem extends ItemStack {
    private UHCGame game;

    public static final String PERSISTENCE_DATA_KEY = "inventory_menu_action";

    UHCMenuItem(UHCGame game, Material type, String urlNext) {
        super(type);

        if (game == null) {
            throw new IllegalArgumentException("Game cannot be null");
        }
        this.game = game;

        if (urlNext == null) {
            throw new IllegalArgumentException("URL cannot be null");
        }

        ItemMeta meta = getItemMeta();
        meta.getPersistentDataContainer().set(new NamespacedKey(this.game.getPlugin(), PERSISTENCE_DATA_KEY),
                PersistentDataType.STRING, urlNext);
        setItemMeta(meta);
    }
}