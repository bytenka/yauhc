package net.yauhc.menu;

import org.bukkit.Material;
import org.bukkit.inventory.meta.ItemMeta;

import net.yauhc.game.UHCGame;
import net.yauhc.lang.UHCLocStr;
import net.yauhc.lang.UHCLocale;
import net.yauhc.player.UHCPlayer;

public abstract class UHCMenu {
    public static final char MENU_DELIMITER = ',';
    public static final char ARGS_DELIMITER = ';';

    protected UHCGame game;
    protected UHCMenu previous;
    protected UHCMenuItem goBack;
    protected UHCMenuItem decoration;

    public UHCMenu(final UHCGame game, final UHCMenu previous) {
        if (game == null) {
            throw new IllegalArgumentException("Game cannot be null");
        }
        this.game = game;
        this.previous = previous;
    }

    void open(UHCPlayer player, ActionType actionType, AccessRight accessRight) {
        this.open(player, actionType, accessRight, new String[] {});
    }

    void open(UHCPlayer player, ActionType actionType, AccessRight accessRight, String[] args) {
        if (getAccessRight() == AccessRight.ADMIN && accessRight != AccessRight.ADMIN) {
            // No
            return;
        }

        UHCLocale tr = player.getLocale();

        this.decoration = new UHCMenuItem(this.game,Material.WHITE_STAINED_GLASS_PANE, ""); // Empty url = no action
        ItemMeta decorationMeta = this.decoration.getItemMeta();
        decorationMeta.setDisplayName(" ");
        this.decoration.setItemMeta(decorationMeta);

        this.goBack = new UHCMenuItem(this.game, Material.ARROW, this.previous == null ? "" : previous.getURL()); 
        ItemMeta goBackMeta = this.goBack.getItemMeta();
        goBackMeta.setDisplayName(new UHCLocStr("menu", "action", "go_back").localize(tr).toString());
        this.goBack.setItemMeta(goBackMeta);

        switch (actionType) {
            case PRIMARY:
                openPrimary(player, args);
                break;
            case SECONDARY:
                openSecondary(player, args);
                break;
            default:
                throw new IllegalArgumentException("Unknown action type");

        }
    }

    String getURL() {
        return this.getURL(new String[] {});
    }

    String getURL(String ...args) {
        if (args == null) {
            throw new IllegalArgumentException("Args cannot be null")
;        }

        StringBuffer buffer = new StringBuffer();
        if (this.previous != null) {
            buffer.append(this.previous.getURL() + MENU_DELIMITER);
        }

        buffer.append(this.getClass().getName());

        if (args.length > 0) {
            buffer.append(ARGS_DELIMITER + String.join(String.valueOf(ARGS_DELIMITER), args));
        }

        return buffer.toString();
    }

    abstract AccessRight getAccessRight();

    abstract UHCMenuItem getSpawnItem(UHCPlayer player);

    protected abstract void openPrimary(UHCPlayer player, String[] args);

    protected abstract void openSecondary(UHCPlayer player, String[] args);

    public enum ActionType {
        PRIMARY, SECONDARY
    }

    public enum AccessRight {
        ADMIN, PLAYER
    }
}