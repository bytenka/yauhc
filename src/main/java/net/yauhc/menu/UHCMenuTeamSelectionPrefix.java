package net.yauhc.menu;

import java.util.ArrayList;
import java.util.Collection;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.meta.ItemMeta;

import net.yauhc.game.UHCGame;
import net.yauhc.lang.UHCLocStr;
import net.yauhc.lang.UHCLocale;
import net.yauhc.player.UHCPlayer;
import net.yauhc.player.UHCTeam;

public class UHCMenuTeamSelectionPrefix extends UHCMenu {

    public UHCMenuTeamSelectionPrefix(UHCGame game, UHCMenu previous) {
        super(game, previous);
    }

    @Override
    AccessRight getAccessRight() {
        return AccessRight.PLAYER;
    }

    @Override
    UHCMenuItem getSpawnItem(UHCPlayer player) {
        return null;
    }

    @Override
    protected void openPrimary(UHCPlayer player, String[] args) {
        if (args.length < 1) {
            throw new IllegalArgumentException("Missing action");
        }

        if (args.length < 2) {
            Inventory inv = createInventory(player, UHCTeam.stringToColor(args[0]));
            player.getPlayer().openInventory(inv);
        } else {
            UHCTeam.Color newTeamColor = UHCTeam.stringToColor(args[0]);
            UHCTeam.Prefix newTeamPrefix = UHCTeam.stringToPrefix(args[1]);

            this.game.getTeamManager().setPlayerTeam(player,
                    new UHCTeam(UHCTeam.Type.REGULAR, newTeamPrefix, newTeamColor));
            player.getPlayer().closeInventory();
        }
    }

    @Override
    protected void openSecondary(UHCPlayer player, String[] args) {
        openPrimary(player, args);
    }

    private Inventory createInventory(UHCPlayer player, UHCTeam.Color color) {
        UHCLocale tr = player.getLocale();

        Inventory inventory = Bukkit.createInventory(player.getPlayer(), 27,
                new UHCLocStr("menu", "team", "selection", "prefix", "title").localize(tr).toString());

        inventory.setItem(11, createTeamBanner(UHCTeam.Prefix.SPADE, color, tr));
        inventory.setItem(12, createTeamBanner(UHCTeam.Prefix.CLUB, color, tr));
        inventory.setItem(13, createTeamBanner(UHCTeam.Prefix.HEART, color, tr));
        inventory.setItem(14, createTeamBanner(UHCTeam.Prefix.DIAMOND, color, tr));
        inventory.setItem(15, createTeamBanner(UHCTeam.Prefix.CROSS, color, tr));

        inventory.setItem(22, this.goBack);

        // Custom decoration if in team
        UHCTeam playerTeam = player.getTeam();
        if (playerTeam != null) {
            this.decoration.setType(UHCTeam.colorToStainedGlassPane(playerTeam.getColor()));
            ItemMeta decorationMeta = this.decoration.getItemMeta();
            decorationMeta.setDisplayName(
                    UHCTeam.colorToChatColor(playerTeam.getColor()) + UHCTeam.prefixToString(playerTeam.getPrefix()));
            this.decoration.setItemMeta(decorationMeta);
        }
        int[] decorationPos = { 0, 1, 7, 8, 9, 17, 18, 19, 25, 26 };
        for (int i : decorationPos) {
            inventory.setItem(i, this.decoration);
        }

        return inventory;
    }

    private UHCMenuItem createTeamBanner(UHCTeam.Prefix prefix, UHCTeam.Color color, UHCLocale tr) {
        UHCMenuItem item = new UHCMenuItem(this.game, UHCTeam.colorToBanner(color),
                getURL(UHCTeam.colorToString(color), UHCTeam.prefixToString(prefix)));
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(String.format("%s%s %s%s", UHCTeam.colorToChatColor(color), UHCTeam.prefixToString(prefix),
                new UHCLocStr("menu", "team", "selection", "prefix", "select", "name").localize(tr)
                        .format(new UHCLocStr("game", "team", "color", UHCTeam.colorToString(color)).localize(tr)),
                ChatColor.RESET));

        ArrayList<String> lore = new ArrayList<>();
        Collection<UHCPlayer> playersInTeam = this.game.getTeamManager()
                .getTeamPlayers(new UHCTeam(UHCTeam.Type.REGULAR, prefix, color));
        if (!playersInTeam.isEmpty()) {
            lore.add("");
            for (UHCPlayer player : playersInTeam) {
                lore.add(String.format("%s%s - %s%s%s", ChatColor.WHITE, ChatColor.BOLD, ChatColor.AQUA,
                        player.getPlayer().getName(), ChatColor.RESET));
            }
        }
        lore.add("");

        // ▶ = U+25B6
        lore.add(ChatColor.GRAY + "▶ "
                + new UHCLocStr("menu", "team", "selection", "prefix", "select", "lore").localize(tr));
        meta.setLore(lore);

        item.setItemMeta(meta);
        return item;
    }

}