package net.yauhc.menu;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.regex.Pattern;

import net.yauhc.game.UHCGame;
import net.yauhc.menu.UHCMenu.AccessRight;
import net.yauhc.player.UHCPlayer;

public class UHCMenuManager {
    private UHCGame game;

    public UHCMenuManager(UHCGame game) {
        if (game == null) {
            throw new IllegalArgumentException("Game cannot be null");
        }
        this.game = game;
    }

    public void openMenu(UHCPlayer player, String url, UHCMenu.ActionType actionType) {
        if (url == null) {
            throw new IllegalArgumentException("URL cannot be null");
        }

        if (url.isEmpty()) {
            return;
        }

        UHCMenu menu = getMenuFromURL(url);
        String[] args = getArgsFromURL(url);

        UHCMenu.AccessRight accessRight = player.getPlayer().isOp() ? AccessRight.ADMIN : AccessRight.PLAYER;
        menu.open(player, actionType, accessRight, args);
    }

    public UHCMenuItem getMenuSpawnItem(Class<? extends UHCMenu> menuClass, UHCPlayer player) {
        return getMenuFromClassName(menuClass.getName(), null).getSpawnItem(player);
    }

    private UHCMenu getMenuFromURL(String url) {
        if (url == null) {
            throw new IllegalArgumentException("URL cannot be null");
        }

        int splitter = url.indexOf(UHCMenu.ARGS_DELIMITER);
        String path = splitter == -1 ? url : url.substring(0, splitter);

        UHCMenu prev = null;
        for (String p : path.split(Pattern.quote(String.valueOf(UHCMenu.MENU_DELIMITER)))) {
            prev = getMenuFromClassName(p, prev);
        }

        return prev;
    }

    private String[] getArgsFromURL(String url) {
        if(url == null) {
            throw new IllegalArgumentException("URL cannot be null");
        }

        int splitter = url.indexOf(UHCMenu.ARGS_DELIMITER);
        if (splitter == -1)
            return new String[] {};

        String path = url.substring(splitter + 1);
        return path.split(Pattern.quote(String.valueOf(UHCMenu.ARGS_DELIMITER)));
    }

    UHCMenu getMenuFromClassName(String className, UHCMenu prev) {
        if(className == null) {
            throw new IllegalArgumentException("Class name cannot be null");
        }

        if(className.isEmpty()) {
            throw new IllegalArgumentException("Class name cannot be empty");
        }

        try {
            Class<?> menuClass = Class.forName(className);
            Constructor<?> menuConstructor = menuClass.getConstructor(UHCGame.class, UHCMenu.class);
            return (UHCMenu) menuConstructor.newInstance(this.game, prev);
        } catch (NoSuchMethodException e) {
            throw new IllegalStateException("Programming error: no visible matching constructor", e);
        } catch (IllegalAccessException e) {
            throw new IllegalStateException("Programming error: constructor is not visible", e);
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException("Failed to find menu class", e);
        } catch (InvocationTargetException | InstantiationException e) {
            throw new IllegalStateException("Something went wrong", e);
        }
    }
}