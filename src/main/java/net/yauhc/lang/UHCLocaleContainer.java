package net.yauhc.lang;

import java.util.HashMap;

public class UHCLocaleContainer {
    private static HashMap<String, UHCLocale> cache = new HashMap<>();
    private static UHCLocale defaultLocale = null;

    private UHCLocaleContainer() {
    }

    public static UHCLocale getLocale(String localeName) {
        // Make sure the default locale has been initialized
        if (defaultLocale == null) {
            defaultLocale = new UHCLocale("en_us", null);
        }
        
        if (localeName == null) {
            return null;
        }
        
        UHCLocale l = cache.get(localeName);
        if (l == null) {
            l = new UHCLocale(localeName, defaultLocale);
            cache.put(localeName, l);
        }
        return l;
    }
}