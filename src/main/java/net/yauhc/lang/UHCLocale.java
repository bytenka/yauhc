package net.yauhc.lang;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.json.JSONObject;
import org.json.JSONTokener;

public class UHCLocale {
    private final String name;
    private final JSONObject entry;

    UHCLocale(final String name, final UHCLocale fallbackOnError) {
        if (name == null) {
            throw new IllegalArgumentException("Name cannot be null");
        }
        this.name = name;

        JSONObject data = null;
        try (InputStream stream = this.getClass().getResourceAsStream(name + ".json")) {
            if (stream != null) {
                try (InputStreamReader reader = new InputStreamReader(stream, "UTF-8")) {
                    final JSONTokener tokener = new JSONTokener(reader);
                    data = new JSONObject(tokener);
                }
            }
        } catch (final IOException ex) {
            data = fallbackOnError != null ? fallbackOnError.getEntry() : new JSONObject();
        }

        this.entry = data;
    }

    JSONObject getEntry() {
        return this.entry;
    }

    public String getName() {
        return this.name;
    }
}