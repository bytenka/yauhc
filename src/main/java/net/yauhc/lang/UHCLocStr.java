package net.yauhc.lang;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.json.JSONObject;

public class UHCLocStr {
    public static final char CHAR_FORMAT = '%';
    public static final char CHAR_COLOR = '&';

    private String str = null;
    private String[] path = null;
    private UHCLocStr[] components = null;

    private UHCLocale locale;
    private UHCLocStr[] args = null;

    public UHCLocStr(String str) {
        if (str == null) {
            throw new IllegalArgumentException("String cannot be null");
        }
        this.str = str;
    }

    public UHCLocStr(String... path) {
        if (path == null) {
            throw new IllegalArgumentException("Path cannot be null");
        }
        this.path = path;
    }

    public UHCLocStr(UHCLocStr... components) {
        if (components == null) {
            throw new IllegalArgumentException("Components cannot be null");
        }
        this.components = components;
    }

    public UHCLocStr localize(String localeName) {
        this.locale = UHCLocaleContainer.getLocale(localeName);
        return this;
    }

    public UHCLocStr localize(UHCLocale locale) {
        this.locale = locale;
        return this;
    }

    public UHCLocStr format(String... args) {
        ArrayList<UHCLocStr> strs = new ArrayList<>(args.length);
        for (String s : args) {
            strs.add(new UHCLocStr(s));
        }
        this.args = strs.toArray(new UHCLocStr[0]);
        return this;
    }


    public UHCLocStr format(UHCLocStr... args) {
        this.args = args;
        return this;
    }

    @Override
    public String toString() {
        if (str != null) {
            // It's a string litteral, return it
            return str;
        } else if (path != null) {
            // Resolve the path and return
            StringBuffer tr;
            try {
                if (this.locale == null)
                    throw new RuntimeException();

                JSONObject current = this.locale.getEntry();
                for (int i = 0; i < this.path.length - 1; i++) {
                    current = current.getJSONObject(this.path[i]);
                }

                tr = new StringBuffer(current.getString(path[path.length - 1]));
                int argsCounter = this.args == null ? -1 : this.args.length - 1;
                for (int i = tr.length() - 2; i >= 0; i--) {
                    char c = tr.charAt(i);
                    if (c == CHAR_COLOR) {
                        ChatColor color = chatToChatColor(tr.charAt(i + 1));
                        tr.replace(i, i + 2, color.toString());
                        continue;
                    }

                    if (c == CHAR_FORMAT) {
                        String str = this.args == null || argsCounter < 0 ? "" : this.args[argsCounter--].localize(this.locale).toString();
                        tr.replace(i, i + 2, str);
                        continue;
                    }
                }

            } catch (RuntimeException e) {
                tr = new StringBuffer(String.join(".", this.path)); // "Pretty" format for debugging
            }
            return tr.toString();

        } else {
            // Resolve and join components
            StringBuffer buffer = new StringBuffer();
            for (UHCLocStr s : this.components) {
                buffer.append(s.localize(this.locale).toString());
            }
            return buffer.toString();
        }
    }

    private ChatColor chatToChatColor(char c) {
        switch (c) {
            case '0':
                return ChatColor.BLACK;
            case '1':
                return ChatColor.DARK_BLUE;
            case '2':
                return ChatColor.DARK_GREEN;
            case '3':
                return ChatColor.DARK_AQUA;
            case '4':
                return ChatColor.DARK_RED;
            case '5':
                return ChatColor.DARK_PURPLE;
            case '6':
                return ChatColor.GOLD;
            case '7':
                return ChatColor.GRAY;
            case '8':
                return ChatColor.DARK_GRAY;
            case '9':
                return ChatColor.BLUE;
            case 'a':
                return ChatColor.GREEN;
            case 'b':
                return ChatColor.AQUA;
            case 'c':
                return ChatColor.RED;
            case 'd':
                return ChatColor.LIGHT_PURPLE;
            case 'e':
                return ChatColor.YELLOW;
            case 'f':
                return ChatColor.WHITE;
            case 'k':
                return ChatColor.MAGIC;
            case 'm':
                return ChatColor.STRIKETHROUGH;
            case 'o':
                return ChatColor.ITALIC;
            case 'l':
                return ChatColor.BOLD;
            case 'n':
                return ChatColor.UNDERLINE;
            case 'r':
                return ChatColor.RESET;
            default:
                throw new IllegalArgumentException("Unknown chat color character");
        }
    }
}