package net.yauhc.command;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.yauhc.main.UHCPlugin;
import net.yauhc.player.UHCPlayer;
import net.yauhc.chat.UHCChatManager;
import net.yauhc.lang.UHCLocStr;
import net.yauhc.lang.UHCLocale;

public class UHCCommandT extends UHCCommand {

    public UHCCommandT(UHCPlugin plugin) {
        super(plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length >= 1) {
            String message = String.join(" ", args);
            Player player = Bukkit.getPlayer(sender.getName());
            if (player == null) {
                // No translation available here, but don't care because it's admin stuff
                sender.sendMessage("Only players can run this command");
                return true;
            }

            UHCPlayer uhcPlayer = this.plugin.getGame().getPlayerManager().getPlayer(player);
            if (uhcPlayer == null) {
                throw new IllegalStateException("Programming error: player not found in player manager");
            }

            if (uhcPlayer.getMoleTeam() == null) {
                UHCLocale tr = uhcPlayer.getLocale();
                sender.sendMessage(new UHCLocStr("chat", "error", "command", "require_being_mole").localize(tr).toString());
                return true;
            } 

            this.plugin.getGame().getChatManager().sendPlayerMessage(uhcPlayer, UHCChatManager.Channel.MOLE_TEAM, message);
        } else {
            // No message
        }

        return true;
    }
}