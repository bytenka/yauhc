package net.yauhc.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import net.yauhc.main.UHCPlugin;
import net.yauhc.game.UHCGame;
import net.yauhc.game.option.UHCGameOptionException;

public class UHCCommandYauhc extends UHCCommand {

    public UHCCommandYauhc(UHCPlugin plugin) {
        super(plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        UHCGame game = this.plugin.getGame();

        if (args.length >= 1) {
            switch (args[0]) {
                case "start": {
                    if (game.isStarted()) {
                        sender.sendMessage("La partie est déjà en cours"); // TODO tr
                    } else {
                        game.start(sender);
                    }
                    break;
                }

                case "stop": {
                    game.stop();
                    break;
                }

                case "save": {
                    try {
                        game.getOptionManager().saveConfig("default");
                    } catch (UHCGameOptionException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    break;
                }

                case "load": {
                    try {
                        game.getOptionManager().loadConfig("default");
                    } catch (UHCGameOptionException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    break;
                }

                default:
                    sender.sendMessage("Commande inconnue " + args[0]); // TODO tr
                    break;
            }
        } else {
            sender.sendMessage("Pas assez d'arguments dans la commande"); // TODO tr
        }

        return true;
    }
}