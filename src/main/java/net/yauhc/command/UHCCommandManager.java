package net.yauhc.command;

import net.yauhc.main.UHCPlugin;

public class UHCCommandManager {
    private UHCPlugin plugin;

    private UHCCommand commandYauhc;
    private UHCCommand commandT;

    public UHCCommandManager(UHCPlugin plugin) {
        if (plugin == null) {
            throw new IllegalArgumentException("Plugin cannot be null");
        }

        this.plugin = plugin;

        this.commandYauhc = new UHCCommandYauhc(this.plugin);
        this.plugin.getCommand("yauhc").setExecutor(this.commandYauhc);

        this.commandT = new UHCCommandT(this.plugin);
        this.plugin.getCommand("t").setExecutor(this.commandT);
    }
}