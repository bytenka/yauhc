package net.yauhc.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import net.yauhc.main.UHCPlugin;

public abstract class UHCCommand implements CommandExecutor {
    protected UHCPlugin plugin;

    public UHCCommand(UHCPlugin plugin) {
        if (plugin == null) {
            throw new IllegalArgumentException("Plugin cannot be null");
        }

        this.plugin = plugin;
    }

    @Override
    public abstract boolean onCommand(CommandSender sender, Command command, String label, String[] args);
}