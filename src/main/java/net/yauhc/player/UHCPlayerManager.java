package net.yauhc.player;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import net.yauhc.game.UHCGame;
import net.yauhc.player.UHCPlayer.GameRole;

public class UHCPlayerManager {
    private UHCGame game;

    private HashMap<UUID, UHCPlayer> players;

    public UHCPlayerManager(UHCGame game) {
        if (game == null) {
            throw new IllegalArgumentException("Game cannot be null");
        }
        this.game = game;

        this.players = new HashMap<>();
    }

    public void clear() {
        this.players.clear();
    }

    public UHCPlayer addPlayer(Player player, UHCPlayer.GameRole role) {
        if (player == null) {
            throw new IllegalArgumentException("Player cannot be null");
        }

        if (role == null) {
            throw new IllegalArgumentException("Role cannot be null");
        }

        UHCPlayer uhcPlayer = new UHCPlayer(player, role);
        if (this.players.putIfAbsent(player.getUniqueId(), uhcPlayer) != null) {
            throw new IllegalArgumentException("Player already exists");
        }

        return uhcPlayer;
    }

    public void removePlayer(Player player) {
        if (player == null)
            return;

        this.players.remove(player.getUniqueId());
    }

    public void setPlayerRole(UHCPlayer player, GameRole role) {
        player.setRole(role);
        this.game.getCurrentPhase().onPlayerChangeRole(player);
    }

    public UHCPlayer getPlayer(Player player) {
        return this.players.get(player.getUniqueId());
    }

    public Collection<UHCPlayer> getPlayers() {
        return this.players.values();
    }

    public Collection<UHCPlayer> getGamers() {
        ArrayList<UHCPlayer> gamers = new ArrayList<>(this.players.size());
        for (UHCPlayer p : this.players.values()) {
            if (p.getRole() == GameRole.PLAYER)
                gamers.add(p);
        }
        return gamers;
    }

    public Collection<UHCPlayer> getSpectators() {
        ArrayList<UHCPlayer> spectators = new ArrayList<>(this.players.size());
        for (UHCPlayer p : this.players.values()) {
            if (p.getRole() == GameRole.SPECTATOR)
                spectators.add(p);
        }
        return spectators;
    }
}