package net.yauhc.player;

import java.util.Objects;

import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;

public class UHCTeam {
    private final Type type;
    private final Prefix prefix;
    private final Color color;

    public UHCTeam(Type type, Prefix prefix, Color color) {
        if (type == null) {
            throw new IllegalArgumentException("Type cannot be null");
        }
        this.type = type;

        if (this.type == Type.MOLE && prefix == null) {
            throw new IllegalArgumentException("Prefix cannot be null if Type == MOLE");
        }
        this.prefix = prefix;

        if (this.type == Type.MOLE && color == null) {
            throw new IllegalArgumentException("Color cannot be null if Type == MOLE");
        }
        this.color = color;
    }

    public Type getType() {
        return this.type;
    }

    public Prefix getPrefix() {
        return this.prefix;
    }

    public Color getColor() {
        return this.color;
    }

    public enum Type {
        REGULAR, MOLE
    };

    public enum Prefix {
        SPADE, CLUB, HEART, DIAMOND, CROSS
    };

    public enum Color {
        DARK_RED, DARK_GREEN, DARK_BLUE, ORANGE, PURPLE,

        RED, GREEN, BLUE, YELLOW, MAGENTA
    };

    @Override
    public boolean equals(Object other) {
        try {
            UHCTeam o = (UHCTeam) other;
            return o != null && this.type == o.type && this.prefix == o.prefix && this.color == o.color;
        } catch (ClassCastException ex) {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.type, this.prefix, this.color);
    }

    public static String typeToString(Type type) {
        switch (type) {
            case REGULAR:
                return "regular";
            case MOLE:
                return "mole";
            default:
                throw new IllegalArgumentException("Unknown type");
        }
    }

    public static Type stringToType(String string) {
        switch (string) {
            case "regular":
                return Type.REGULAR;
            case "mole":
                return Type.MOLE;
            default:
                throw new IllegalArgumentException("Unknown string type");
        }
    }

    public static String prefixToString(Prefix prefix) {
        switch (prefix) {
            case SPADE:
                return "♠"; // U+2660
            case CLUB:
                return "♣"; // U+2663
            case HEART:
                return "♥"; // U+2665
            case DIAMOND:
                return "♦"; // U+2666
            case CROSS:
                return "❌"; // U+274C
            default:
                throw new IllegalArgumentException("Unknown prefix");
        }
    }

    public static Prefix stringToPrefix(String string) {
        switch (string) {
            case "♠": // U+2660
                return Prefix.SPADE;
            case "♣": // U+2663
                return Prefix.CLUB;
            case "♥": // U+2665
                return Prefix.HEART;
            case "♦": // U+2666
                return Prefix.DIAMOND;
            case "❌": // U+274C
                return Prefix.CROSS;
            default:
                throw new IllegalArgumentException("Unknown string prefix");
        }
    }

    public static ChatColor colorToChatColor(Color color) {
        switch (color) {
            case DARK_RED:
                return ChatColor.DARK_RED;
            case DARK_GREEN:
                return ChatColor.DARK_GREEN;
            case DARK_BLUE:
                return ChatColor.DARK_BLUE;
            case ORANGE:
                return ChatColor.GOLD;
            case PURPLE:
                return ChatColor.DARK_PURPLE;
            case RED:
                return ChatColor.RED;
            case GREEN:
                return ChatColor.GREEN;
            case BLUE:
                return ChatColor.BLUE;
            case YELLOW:
                return ChatColor.YELLOW;
            case MAGENTA:
                return ChatColor.LIGHT_PURPLE;
            default:
                throw new IllegalArgumentException("Unknown color");
        }
    }

    public static DyeColor colorToDyeColor(Color color) {
        switch (color) {
            case DARK_RED:
                return DyeColor.RED;
            case DARK_GREEN:
                return DyeColor.GREEN;
            case DARK_BLUE:
                return DyeColor.BLUE;
            case ORANGE:
                return DyeColor.ORANGE;
            case PURPLE:
                return DyeColor.PURPLE;
            case RED:
                return DyeColor.PINK;
            case GREEN:
                return DyeColor.LIME;
            case BLUE:
                return DyeColor.LIGHT_BLUE;
            case YELLOW:
                return DyeColor.YELLOW;
            case MAGENTA:
                return DyeColor.MAGENTA;
            default:
                throw new IllegalArgumentException("Unknown color");
        }
    }

    public static Material colorToBanner(Color color) {
        switch (color) {
            case DARK_RED:
                return Material.RED_BANNER;
            case DARK_GREEN:
                return Material.GREEN_BANNER;
            case DARK_BLUE:
                return Material.BLUE_BANNER;
            case ORANGE:
                return Material.ORANGE_BANNER;
            case PURPLE:
                return Material.PURPLE_BANNER;
            case RED:
                return Material.PINK_BANNER;
            case GREEN:
                return Material.LIME_BANNER;
            case BLUE:
                return Material.LIGHT_BLUE_BANNER;
            case YELLOW:
                return Material.YELLOW_BANNER;
            case MAGENTA:
                return Material.MAGENTA_BANNER;
            default:
                throw new IllegalArgumentException("Unknown color");
        }
    }

    public static Material colorToStainedGlass(Color color) {
        switch (color) {
            case DARK_RED:
                return Material.RED_STAINED_GLASS;
            case DARK_GREEN:
                return Material.GREEN_STAINED_GLASS;
            case DARK_BLUE:
                return Material.BLUE_STAINED_GLASS;
            case ORANGE:
                return Material.ORANGE_STAINED_GLASS;
            case PURPLE:
                return Material.PURPLE_STAINED_GLASS;
            case RED:
                return Material.PINK_STAINED_GLASS;
            case GREEN:
                return Material.LIME_STAINED_GLASS;
            case BLUE:
                return Material.LIGHT_BLUE_STAINED_GLASS;
            case YELLOW:
                return Material.YELLOW_STAINED_GLASS;
            case MAGENTA:
                return Material.MAGENTA_STAINED_GLASS;
            default:
                throw new IllegalArgumentException("Unknown color");
        }
    }

    public static Material colorToStainedGlassPane(Color color) {
        switch (color) {
            case DARK_RED:
                return Material.RED_STAINED_GLASS_PANE;
            case DARK_GREEN:
                return Material.GREEN_STAINED_GLASS_PANE;
            case DARK_BLUE:
                return Material.BLUE_STAINED_GLASS_PANE;
            case ORANGE:
                return Material.ORANGE_STAINED_GLASS_PANE;
            case PURPLE:
                return Material.PURPLE_STAINED_GLASS_PANE;
            case RED:
                return Material.PINK_STAINED_GLASS_PANE;
            case GREEN:
                return Material.LIME_STAINED_GLASS_PANE;
            case BLUE:
                return Material.LIGHT_BLUE_STAINED_GLASS_PANE;
            case YELLOW:
                return Material.YELLOW_STAINED_GLASS_PANE;
            case MAGENTA:
                return Material.MAGENTA_STAINED_GLASS_PANE;
            default:
                throw new IllegalArgumentException("Unknown color");
        }
    }

    public static String colorToString(Color color) {
        switch (color) {
            case DARK_RED:
                return "dark_red";
            case DARK_GREEN:
                return "dark_green";
            case DARK_BLUE:
                return "dark_blue";
            case ORANGE:
                return "orange";
            case PURPLE:
                return "purple";
            case RED:
                return "red";
            case GREEN:
                return "green";
            case BLUE:
                return "blue";
            case YELLOW:
                return "yellow";
            case MAGENTA:
                return "magenta";
            default:
                throw new IllegalArgumentException("Unknown color");
        }
    }

    public static Color stringToColor(String string) {
        switch (string) {
            case "dark_red":
                return Color.DARK_RED;
            case "dark_green":
                return Color.DARK_GREEN;
            case "dark_blue":
                return Color.DARK_BLUE;
            case "orange":
                return Color.ORANGE;
            case "purple":
                return Color.PURPLE;
            case "red":
                return Color.RED;
            case "green":
                return Color.GREEN;
            case "blue":
                return Color.BLUE;
            case "yellow":
                return Color.YELLOW;
            case "magenta":
                return Color.MAGENTA;
            default:
                throw new IllegalArgumentException("Unknown string color");
        }
    }
}