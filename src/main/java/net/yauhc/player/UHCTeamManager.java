package net.yauhc.player;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

import org.bukkit.ChatColor;

import net.yauhc.game.UHCGame;
import net.yauhc.lang.UHCLocStr;
import net.yauhc.lang.UHCLocale;

public class UHCTeamManager {
    private UHCGame game;

    private HashMap<UHCTeam, HashSet<UHCPlayer>> teams;
    private HashMap<UHCTeam, HashSet<UHCPlayer>> moleTeams;

    private boolean enabled;

    public UHCTeamManager(UHCGame game) {
        if (game == null) {
            throw new IllegalArgumentException("Game cannot be null");
        }
        this.game = game;

        this.teams = new HashMap<>();
        this.moleTeams = new HashMap<>();

        this.enabled = true;
    }

    public void setTeamEnabled(boolean enabled) {
        if (!enabled) {
            // Remove players from their teams
            for (HashSet<UHCPlayer> team : this.teams.values()) {
                for (UHCPlayer player : team) {
                    setPlayerTeam(player, null);
                }
            }

            for (HashSet<UHCPlayer> moleTeam : this.moleTeams.values()) {
                for (UHCPlayer player : moleTeam) {
                    setPlayerMoleTeam(player, null);
                }
            }
        }

        this.enabled = enabled;
    }

    public boolean getTeamEnabled() {
        return this.enabled;
    }

    public void setPlayerTeam(UHCPlayer player, UHCTeam team) {
        if (!this.enabled) {
            throw new IllegalStateException("Teams are not enabled");
        }

        if (player == null) {
            throw new IllegalArgumentException("Player cannot be null");
        }

        if (team != null) {
            addPlayerToTeam(player, team);
        } else {
            removePlayerFromTeam(player);
        }

        this.game.getCurrentPhase().onPlayerChangeTeam(player, team);
    }

    public void setPlayerMoleTeam(UHCPlayer player, UHCTeam team) {
        if (!this.enabled) {
            throw new IllegalStateException("Teams are not enabled");
        }

        if (player == null) {
            throw new IllegalArgumentException("Player cannot be null");
        }

        if (team != null) {
            addPlayerToMoleTeam(player, team);
        } else {
            removePlayerFromMoleTeam(player);
        }
    }

    public Collection<UHCPlayer> getTeamPlayers(UHCTeam team) {
        if (team == null) {
            throw new IllegalArgumentException("Team cannot be null");
        }

        HashSet<UHCPlayer> res;
        switch (team.getType()) {
            case REGULAR:
                res = this.teams.get(team);
                break;
            case MOLE:
                res = this.moleTeams.get(team);
                break;
            default:
                throw new IllegalArgumentException("Unhandled team type");
        }
        return res == null ? new HashSet<>() : res;
    }

    public Collection<UHCTeam> getTeams() {
        return this.teams.keySet();
    }

    private void addPlayerToTeam(UHCPlayer player, UHCTeam team) {
        if (player == null) {
            throw new IllegalArgumentException("Player cannot be null");
        }

        if (team == null) {
            throw new IllegalArgumentException("Team cannot be null");
        }

        if (team.getType() != UHCTeam.Type.REGULAR) {
            throw new IllegalArgumentException("Team can only be 'regular' for this function");
        }

        UHCLocale tr = player.getLocale();

        UHCTeam previousTeam = player.getTeam();
        if (previousTeam != null) {
            // Player is already in a team

            if (previousTeam.equals(team)) {
                // Player is already in this exact team

                player.getPlayer()
                        .sendMessage(
                            new UHCLocStr("chat", "team", "on_already_joined").localize(tr).format(
                                String.format("%s%s %s%s",
                                    UHCTeam.colorToChatColor(previousTeam.getColor()),
                                    UHCTeam.prefixToString(previousTeam.getPrefix()),
                                    new UHCLocStr("game", "team", "color", UHCTeam.colorToString(previousTeam.getColor())).localize(tr),
                                    ChatColor.RESET)
                                ).toString()
                        );
                return;

            } else {
                // Player is in another team
                HashSet<UHCPlayer> t = this.teams.get(previousTeam);
                if (t == null) {
                    throw new IllegalStateException(
                            "Programming error: Player said to be in a team but manager says otherwise");
                }

                if (!t.remove(player)) {
                    throw new IllegalStateException("Programming error: Player not found in team set");
                }

                // Now the team may be empty. If it is, we should untrack it
                if (t.isEmpty()) {
                    this.teams.remove(previousTeam);
                }
            }
        }

        HashSet<UHCPlayer> newTeam = this.teams.get(team);
        if (newTeam == null) {
            newTeam = new HashSet<>();
            this.teams.put(team, newTeam);
        }
        newTeam.add(player);
        player.setTeam(team);

        // Player has been added

        // Set name color in TAB
        player.getPlayer()
                .setPlayerListName(String.format(UHCTeam.colorToChatColor(team.getColor()) + "%s %s" + ChatColor.RESET,
                        UHCTeam.prefixToString(team.getPrefix()), player.getPlayer().getName()));

        // Notify player of the change
        player.getPlayer()
        .sendMessage(new UHCLocStr("chat", "team", "on_join").localize(tr).format(
            String.format("%s%s %s%s", UHCTeam.colorToChatColor(team.getColor()), UHCTeam.prefixToString(team.getPrefix()),
            new UHCLocStr("game", "team", "color", UHCTeam.colorToString(team.getColor())).localize(tr), ChatColor.RESET)).toString());
    }

    private void removePlayerFromTeam(UHCPlayer player) {
        if (player == null) {
            throw new IllegalArgumentException("Player cannot be null");
        }

        UHCLocale tr = player.getLocale();

        UHCTeam previousTeam = player.getTeam();
        if (previousTeam == null) {
            // Player already does not have a team
            player.getPlayer().sendMessage(new UHCLocStr("chat", "team", "on_already_left").localize(tr).toString());

        } else {
            HashSet<UHCPlayer> team = this.teams.get(previousTeam);
            if (team == null) {
                throw new IllegalStateException(
                        "Programming error: Player said to be in a team but manager says otherwise");
            }

            if (!team.remove(player)) {
                throw new IllegalStateException("Programming error: Player not found in team set");
            }

            // Now the team may be empty. If it is, we should untrack it
            if (team.isEmpty()) {
                this.teams.remove(previousTeam);
            }

            player.setTeam(null);

            // Removed from team

            // Reset name color in TAB
            player.getPlayer().setPlayerListName(null);

            // Notify player of the change
            player.getPlayer()
            .sendMessage(new UHCLocStr("chat", "team", "on_leave").localize(tr).format(
                String.format("%s%s %s%s", UHCTeam.colorToChatColor(previousTeam.getColor()), UHCTeam.prefixToString(previousTeam.getPrefix()),
                new UHCLocStr("game", "team", "color", UHCTeam.colorToString(previousTeam.getColor())).localize(tr), ChatColor.RESET)).toString());
        }
    }

    private void addPlayerToMoleTeam(UHCPlayer player, UHCTeam moleTeam) {
        if (player == null) {
            throw new IllegalArgumentException("Player cannot be null");
        }

        if (moleTeam == null) {
            throw new IllegalArgumentException("Mole team cannot be null");
        }

        if (moleTeam.getType() != UHCTeam.Type.MOLE) {
            throw new IllegalArgumentException("Team can only be 'mole' for this function");
        }

        UHCLocale tr = player.getLocale();

        UHCTeam previousMoleTeam = player.getMoleTeam();
        if (previousMoleTeam != null) {
            // Player is already in a team

            if (previousMoleTeam.equals(moleTeam)) {
                // Player is already in this exact team
                return;
            } else {
                // Player is in another team
                HashSet<UHCPlayer> t = this.moleTeams.get(previousMoleTeam);
                if (t == null) {
                    throw new IllegalStateException(
                            "Programming error: Player said to be in a mole team but manager says otherwise");
                }

                if (!t.remove(player)) {
                    throw new IllegalStateException("Programming error: Player not found in mole team set");
                }

                // Now the team may be empty. If it is, we should untrack it
                if (t.isEmpty()) {
                    this.moleTeams.remove(previousMoleTeam);
                }
            }
        }

        HashSet<UHCPlayer> newMoleTeam = this.moleTeams.get(moleTeam);
        if (newMoleTeam == null) {
            newMoleTeam = new HashSet<>();
            this.moleTeams.put(moleTeam, newMoleTeam);
        }
        newMoleTeam.add(player);
        player.setMoleTeam(moleTeam);

        // Player has been added
        // TODO notify them with big text and sound and that kind of stuff
    }

    private void removePlayerFromMoleTeam(UHCPlayer player) {
        if (player == null) {
            throw new IllegalArgumentException("Player cannot be null");
        }

        UHCTeam previousMoleTeam = player.getMoleTeam();
        if (previousMoleTeam != null) {
            HashSet<UHCPlayer> team = this.moleTeams.get(previousMoleTeam);
            if (team == null) {
                throw new IllegalStateException(
                        "Programming error: Player said to be in a mole team but manager says otherwise");
            }

            if (!team.remove(player)) {
                throw new IllegalStateException("Programming error: Player not found in mole team set");
            }

            // Now the team may be empty. If it is, we should untrack it
            if (team.isEmpty()) {
                this.moleTeams.remove(previousMoleTeam);
            }

            player.setMoleTeam(null);
        }
    }
}