package net.yauhc.player;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLocaleChangeEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import net.yauhc.game.UHCGame;
import net.yauhc.lang.UHCLocaleContainer;

public class UHCPlayerConnectionListener implements Listener {

    private UHCGame game;

    public UHCPlayerConnectionListener(UHCGame game) {
        if (game == null) {
            throw new IllegalArgumentException("Game cannot be null");
        }

        this.game = game;
    }

    @EventHandler
    public void onPlayerJoinEvent(PlayerJoinEvent event) {
        event.setJoinMessage(ChatColor.GRAY + "(" + ChatColor.DARK_GREEN + "+" + ChatColor.GRAY + ") "
                + event.getPlayer().getDisplayName());

        this.game.getCurrentPhase().onPlayerJoin(event.getPlayer());
    }

    @EventHandler
    public void onPlayerLeaveEvent(PlayerQuitEvent event) {
        event.setQuitMessage(ChatColor.GRAY + "(" + ChatColor.DARK_RED + "-" + ChatColor.GRAY + ") "
                + event.getPlayer().getDisplayName());

        this.game.getCurrentPhase().onPlayerQuit(event.getPlayer());
    }

    @EventHandler
    public void onPlayerLocaleChangeEvent(PlayerLocaleChangeEvent event) {
        UHCPlayerManager playerManager = this.game.getPlayerManager();
        UHCPlayer player = playerManager.getPlayer(event.getPlayer());
        if (player != null) {
            player.setLocale(UHCLocaleContainer.getLocale(event.getLocale()));
            this.game.getCurrentPhase().onPlayerLocaleChange(player);
        }

    }
}