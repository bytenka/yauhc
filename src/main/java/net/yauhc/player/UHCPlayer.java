package net.yauhc.player;

import org.bukkit.entity.Player;

import net.yauhc.lang.UHCLocale;
import net.yauhc.lang.UHCLocaleContainer;

public class UHCPlayer {
    private Player player;
    private GameRole role;
    private UHCLocale locale;

    private UHCTeam team = null;
    private UHCTeam moleTeam = null;
    private boolean revealed = false;

    UHCPlayer(Player player, GameRole role) {
        if (player == null) {
            throw new IllegalArgumentException("Player cannot be null");
        }
        this.player = player;

        if (role == null) {
            throw new IllegalArgumentException("Role cannot be null");
        }
        this.role = role;

        this.locale = UHCLocaleContainer.getLocale(player.getLocale());
    }

    public void setPlayer(Player player) {
        if (player == null) {
            throw new IllegalArgumentException("Player cannot be null");
        }
        this.player = player;
    }

    public Player getPlayer() {
        return this.player;
    }

    public GameRole getRole() {
        return this.role;
    }

    public UHCLocale getLocale() {
        return this.locale;
    }

    public void setLocale(UHCLocale locale) {
        if (locale == null) {
            throw new IllegalArgumentException("Locale cannot be null");
        }
        this.locale = locale;
    }

    void setRole(GameRole role) {
        this.role = role;
    }

    public UHCTeam getTeam() {
        return this.team;
    }

    void setTeam(UHCTeam team) {
        this.team = team;
    }

    public UHCTeam getMoleTeam() {
        return this.moleTeam;
    }

    void setMoleTeam(UHCTeam moleTeam) {
        this.moleTeam = moleTeam;
    }

    public boolean isRevealed() {
        return this.moleTeam != null && this.revealed;
    }

    public void setRevealed(boolean revealed) {
        if (this.moleTeam == null && revealed) {
            throw new IllegalArgumentException("Cannot set player as revealed because player is not a mole");
        }
        this.revealed = revealed;
    }

    public enum GameRole {
        PLAYER, SPECTATOR,
    }

    @Override
    public boolean equals(Object other) {
        try {
            UHCPlayer p = (UHCPlayer) other;
            return other != null && this.player.getUniqueId().equals(p.player.getUniqueId());

        } catch (ClassCastException e) {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return this.player.getUniqueId().toString().hashCode();
    }
}