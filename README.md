# Yauhc
*Actually idk how to call the project*

### Build
This is a Maven project. Being a plugin, external dependencies are bundled with the JAR.

Create the JAR with dependencies: `mvn package assembly:single`

This will create both a `*.jar` and a `*-jar-with-dependencies.jar` under the `target/` folder. Use the last one.

### Contribute
Feel free to submit pull requests, especially for bug fixes and missing translations. Keep in mind that new functionalities or big changes to the code base will not be accepted without proper argumentation.

#### Translate
Translation files can be found under `src/resources/net/yauhc/lang`. The locale file `en_us.json` is the fallback language when a field cannot be found for user's locale, so this file should always be kept up-to-date. If you want to contribute and add a new locale, just copy `en_us.json`, rename according to [minecraft's locale code list](https://minecraft.gamepedia.com/Language#Available_languages), and you're good to go.